# ChangeLog

## Release 2.3.7

### GUI 
  * Fix a crash in upgrading MorpheusML3 models with InitCellObject
  * Enable GUI builds without SBML support

### MorpheusML
  * Add support for random numbers from inverse gamma function

### Simulator
  * Fix a regression in GnuPlotter pallette range scaling
  * Constrain MembraneProperty to boundary nodes
  * Fix CPM yield threadsave evaluation

## Release 2.3.5

### GUI
  * Fixed loading tag selection for model graph rendering
  * Small fix for failing to loading some disabled elements

### MorpheusML
  * InitCellObject gained **hexagon/honeycomb shapes** and supports object rotation now
  * Support expressions in StartTime and StopTime specifications
  
### Simulator
  * Fixed rare bug claiming unable to remove a node from cell
  * Performance improvements for the PDE solver
  * Faster memory storage for spatial domains (e.g. for cells)
  * Gnuplotter: create suitible palette for integer value ranges (like cell ids)
  * Logger: Fixes for cell membrane plots
  * Fix VectorMapper not threadsave
  * Fix rare crash on node tracking

## Release 2.3.4

### GUI
  * MorpheusML version upgrades are also applied to disabled model parts

### MorpheusML
  * Added **classic** option for surface length estimation. This mode resembles the CC3D neigborhood and scaling settings
  * Added event **Logging** option to many events (Event, CellDivision, CellDeath, ChangeCellType, AddCell)

### Simulator
  * Fix adjustment of initialisation order of cell properties
  * Fix snapshotting simulations
  * Fix flux boundaries for Fields given by expressions
  * Expression defined Domains added
  * Switch back to official xtensor library version (merged feature)
  


## Release 2.3.3

### GUI
  * Fixed a potential crash when adding model components
  * Morpheus uses by default 50% of the systems core count for a simulation job
  * Generated movies should play again in all browsers
  
### Simulator
  * Improvements of automatic scheduling of Reaction-Diffusion-System on Membranes
  * Added more search places for gnuplot on mac

  
## Release 2.3.2

### GUI
  * Fix a potential crash when loading / executing job queue
  * Update CellPolarity example
  
### MorpheusML
  * Allow negative time values for Start, Stop time

### Simulator
  * Fixed the Diffusion operator on 3D cell Membranes
  * Fix cell center tracking on the new hexagonal lattice

## Release 2.3.1

### GUI
  * Fixed a crash when using the File Chooser in the Attribute editor (MacOS, Windows)
  * Added missing highlighting for some function in the Math editor
  * Work around some quality issues in movie generation, proper cleanup of temporaries
  * Fixed GUI documentation auto-navigation (MacOS)
  * Fixed stopping a set of jobs including pending jobs
  
### MorpheusML
  * Fixed validation pattern for celltype lists

### Simulator
  * Fixed some small bugs in placement of Labels and Isolines
  * Again expose the runtime spent in the computation of diffusion
  * Added missing support for some image formats in tiff library for Windows
  * Fixed a regression in diffusion within domains with noflux boundaries
  

## Release 2.3.0

### GUI
  * **Rerun** button choice to allow reusing the last job id when starting a job.

### MorpheusML
  * Renewed **hexagonal** lattice implementation to have a rectangular shape, old version is retained as 'hexagonal-old'.
  * Support for heterogeneous **Diffusion** and flux boundaries.  Also diffusion is now affected by the time-scaling of the **System** containing a respective **DiffEqn**
  * Support **VolumeConstraint** for **medium** class CellTypes
  * **InterfaceConstraint** can conserve the length of a cell interface with cells of another type
  * **CSVReader** can read cell populations from file.

### Simulator
  * **Parallelized the CPM** algorithm (OMP_NUM_THREADS sets the number of threads)
  * Changed underlying data containers to xtensor library (requires c++14)
  * Further reduced memory footprint
  * Fixed the OME header in the **TiffPlotter**
  * TiffReader now centers the configuration by default when the lattice is larger than the tiff size.
  * Fixed symbol replacement in **External** component
  * Fixed **Gnuplotter** issues with gnuplot >= 5.4.4

## Release 2.2.6

  * Enable build on ARM (e.g. M1)
  * Fix automatic graphviz library fallback 
  
### GUI 
  * Fix crashes on large parameter sweeps

### Simulator
  * Fix ConnectivityConstraint to use the proper Neighborhood
  

## Release 2.2.5

### MorpheusML
  * **CellDivision** now exposes the option to trigger whenever the condition turns true

### Simulator
  * Coordinate conversion of IntermediateVector fixed
  * Ensure cell.orientation is unique (0-pi)
  * Bug fix for the data format descriptor of 16bit TiffPlotter
  * Improve error messages on partially defined symbols

## Release 2.2.4

### GUI
  * Reduce memory consumption of text preview in the job view
  
### Simulator
  * Rework circular reference detection in the initialisation cycle
  * Fix color bar limits in Logger Plots
  * Add command line option to skip any gnuplot tasks (thus does not choke on missing gnuplot)
  * Fix command line parameter override using "--set" option


## Release 2.2.3

### GUI
  * Fixed model graph generator library that may have caused outdated model graphs.

### Simulator
  * Added loop dependency detection in Field initialization
  * Fix loading 8-bit Tiff images
  * Consistently center Tiff image data when smaller than lattice size
  * Fixed regression: Prevent rescheduling of the CPM sampler
  * Fix build in debug mode

## Release 2.2.2

### MorpheusML
  * Merged the **MechanicalLink** component

### GUI
  * Fixed command line parsing of certain url/file references 
  * Fixed checkbox lists on MacOS (issue #223)
  * Preserve XML Comments in MorpheusML models

### Simulator
  * Reenable output of performance statistics, if no json stats are requested.
  * Fixed logging MembraneProperties
  * Fixed certain parallel **Mapper** modes

## Release 2.2.1

### Simulator
  * Fix boost linking for static builds
  * Fix writing performance statistics

## Release 2.2.0

### GUI
  * Introduced an Interactive Configurable Model Graph
  * Support Tagging, Filtering and Sorting of Model Components
  * Switched to Qt5
  * Support reading and writing (gz) compressed models
  * Support for morpheus://... url scheme, that allows loading resources through url links

### MorpheusML
  * The space symbol (Space/SpaceSymbol) now always provides the location in orthogonal coordinates, also on hexagonal lattices.
  * AddCell accepts Count instead of Condition as the number of cells to be placed.
  * ClusterTracker can cluster cells of multiple cell types.
  * Logger
    * gained support for conditional logging using Logger/Restrictions/@condition
    * learned to use discrete colors for integer data
  * Gnuplotter
    * allows to set a z-slice per Plot and also Arrows and Labels follow the z-slize filter.
    * cell opacity moved to Plot/Cells/@opacity
  * Added Populations InitVectorProperty with optional spherical notation
  * Added VectorMapper as a counterpart to the scalar Mapper
  * Lattice size now can also be specified through expressions
  * Contact energies now support expressions with access to symbols of involved cells
  * Rework of XSD specifcation
    * XSD parser supports extension of complexTypes
    * Plugins are extension of base types
    * Registration of plugins as members of <xs:all> groups
  * Added **@tags** and **Annotation** nodes to all model components

### Simulator
  * Performance improvements for Mappers and Reporters using OpenMP parallelization
  * Support reading (gz) compressed models
  * Provide performance statistics as json file using the --perf-stats option
  * Added Test system for full XML models
  * Restructured CMake build to make use of targets and boosted requrements to cmake>=3.3.0
  * Build system now supports building on Windows MSys2 and Mac Homebrew in addition to linux systems.

### Bug Fixes
  * Fix cell property initialization override priority (InitProperty takes highest priority)
  * Fixed diffusion scaling on hexagonal lattices (was raised by factor 0.5)

  
## Release 2.1.1

### Bug fixes
  * Fix a bug in scheduling systems consisting of VectorRules only
  * Fix a rare crash upon ChangeCellType 
  * Fix reading numeric html encoded utf8 characters


## Release 2.1

### GUI
  * Reworked DelayProperty/Variable that allows varying delay times and history initialisation from expression
  * Largely improve SBML import
     * Provide more import target options
     * Add support for multiple compartments and variable size compartments
     * Support delays by via DelayProperties
     * Support Events with delays
     * Support HMC (comp)
  * Fix MacOS crash on double-clicking symbol list
  * Fix Windows SBML support (suitible library build)
  * Fix Windows Job removal to also remove all related files

### MorpheusML
  * Expose local symbols to the input of the Neighborhood(Vector)Reporter
  * Allow constant expressions in time-step specifications
  * MorpheusML version bump 4.0

### Simulator
  * Add adaptive step size ODE solvers: adaptive45 (Dormand-Prince), adaptive45_ck (Cash-Karp), adaptive_23 (Bogacki-Shampine)
  * Renewed implementation of Poissonian Disc Population Initializer
  * Command line option for setting the output directory added
  * Fix rare misplacement in box object initializer
  * Reduce memory footprint

## Patch 2.0.1
  * Fixed VectorRules not working
  * Fixed broken image table for parameter sweeps

## Release 2.0

### MorpheusML
  * The generalized **Mapper** now takes care to map information between spatial contexts, replaces the CellReporter
  * The **Function** plugin now supports parametric functions and function overloading.
  * The new **External** plugin allows to run external code during analysis steps
  * The new **ContactLogger** tracks cell contacts over time
  * The value of a **Constant** can be provided via expression
  * The new **VectorField** can represent spatial vector data
  * Multiple **Population** per CellType are now supported
  * **GnuPlotter** layout adjustments to efficiently support large lattices
  * Binary **VTK** export (performance)
  * Full support for **snapshotting** simulation states
  * Removed any remains of time / space units

### GUI
  * Largely improved inApp documentation
  * Adaptive multiline editor panel for expressions in attributes
  * Reenabled SBML import
  * Allow Copy/Paste & Drag/Drop of external XML model snippets 
  * Optionally preset random seeds for parameter sweeps

### Simulator
  * Many fixes to cell population initializers
  * **Symbolic Linking** infrastructure rewritten to enable extensibility
  * Scheduling fixes for **DelayProperties**
  * **Gnuplotter** and **Logger** now deal better with sub and superscript in symbol names or descriptions
  * **Field boundaries** can be expressions
  * Added **GoogleTest** as testing framework
  * Performance improvements, i.e.
    * Added dynamic EdgeTracker defragmentation
    * Parallelized diffusion in domain Fields
    * Precalculating constant expressions at initialisation
    * ...

### others
  * Tons of compiler compatibility fixes
  * Doxygen issue workarounds
  
## 1.9.3 
  * Initial import

