ARG IMAGE_NAME
FROM $IMAGE_NAME

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y cmake cmake-curses-gui doxygen file g++ git gnuplot libboost-dev libboost-filesystem-dev libboost-program-options-dev libqt5sql5-sqlite libqt5svg5-dev libsbml5-dev libtiff5-dev libxml2-utils qttools5-dev qtwebengine5-dev xsltproc xxd zlib1g-dev