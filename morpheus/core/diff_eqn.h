//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef DIFFERENTIALEQN_H
#define DIFFERENTIALEQN_H

#include "core/interfaces.h"

/**
\defgroup ML_DiffEqn DiffEqn
\ingroup MathExpressions
\ingroup ML_System

Assignment of a rate equation to a symbol.

Ordinary differential equation \f$ \frac{dX}{dt}=a \f$ if \f$ X \f$ is a \ref ML_Variable or a \ref ML_Property.

Partial differential equation \f$ \frac{\partial X}{\partial t}=D_{X}\nabla^2X+a \f$ where \f$ X \f$ is a \ref ML_Field and \f$ D_{X} \f$ is its diffusion coefficient.

DiffEqn are only allowed within \ref ML_System.
**/

/*
\defgroup ML_DiffEqn DiffEqn
\ingroup MathExpressions
\ingroup ML_System

Specify a Reaction-Advection-Diffusion (RAD) system for the symbol referred in \b symbol-ref. 
Spatial operators are only allowed when referring to \ref ML_Field or \ref ML_MembraneProperty.

The reaction part is defined under \b Expression, e.g. the ordinary differential equation \f$ \frac{dX}{dt}=a \f$,  where \f$ X \f$ is a \ref ML_Variable or a \ref ML_Property.


Partial differential equations \f$ \frac{\partial X}{\partial t}=D_{X}\nabla^2X - C_{X}\nabla X - f(X)\f$ where \f$ X \f$ is a \ref ML_Field and \f$ D_{X} \f$ is its diffusion coefficient to be specified under \b Diffusion and \f$ C_{x} \f$ is its advection velocity to be specified under \b Advection.

DiffEqn are only allowed within \ref ML_System.
**/


/** @brief Differential Equation Plugin
 * 
 *  A plain Plugin loading differential  equations from XML.
 *  The enclosing "System" is responsible for the actual computation.
 */

class DifferentialEqn : public Plugin
{
	public:
		struct DiffEqnDesc {
			string symbol;
			string reaction_expression;
			bool has_diffusion = false;
			string diffusion_expression;
			bool has_advection = false;
			string advection_expression;
		};
		DECLARE_PLUGIN("DiffEqn");
		virtual void loadFromXML(const XMLNode, Scope* scope ) override;
		/// This init method is called by PDE_Sim
		virtual void init(const Scope* scope) override;
		
		string getReaction() const { return reaction(); }
		string getSymbol() const { return symbol(); }
		DiffEqnDesc getData() const;
		
	private:
		PluginParameter2<string, XMLValueReader, RequiredPolicy> symbol;
		PluginParameter2<string, XMLValueReader, DefaultValPolicy> reaction;
		PluginParameter2<string, XMLValueReader, OptionalPolicy> diffusion;
		PluginParameter2<bool, XMLValueReader, DefaultValPolicy> well_mixed;
		PluginParameter2<string, XMLValueReader, OptionalPolicy> advection;
		
		CellType* celltype;
		valarray<double> delta_data;
		
};

#endif // DIFFERENTIALEQN_H
