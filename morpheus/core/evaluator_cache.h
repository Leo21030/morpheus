#ifndef EVALUATOR_CACHE_H
#define EVALUATOR_CACHE_H

#include "scope.h"
#include "muParser/muParser.h"
#include <functional>

/**
 * \brief Value cache for the Expression evaluators
 *  
 * * Provides a variable factory based on 
 *   * the local scope symbols
 *   * namespace scopes symbol
 *   * cache local symbols
 *   * (parser variables)
 * * Fetches Symbols from scopes
 * * Stores local symbols
 * * Permits scalar expansion on expressions for vector operands
 * * Efficient assignment of locals through array
 */
struct EvaluatorCacheDesc {
	bool allow_partial = false;
	bool kernel_mode = false;
};

class EvaluatorCache 
{
public:
	
	/// Descriptor for the cache layout of cache local variables.
	struct LocalSymbolDesc {
		string symbol;
		enum Type { DOUBLE, VECTOR } type;
	};
	
	
	EvaluatorCache(const Scope * scope, EvaluatorCacheDesc spec = EvaluatorCacheDesc()) : local_scope(scope), eval_spec(spec) {};
	
	EvaluatorCache(const EvaluatorCache& other) ; // We need to adjust the cache links of namespace symbols.
	
	const EvaluatorCache& operator=(const EvaluatorCache& other) = delete;
	
	const Scope* getScope() { return local_scope; }

	/// Symbol factory to be used with muParser
	static mu::value_type* registerSymbol(const mu::char_type* symbol, void* ptr);
	
	/// Add a local symbol @p name to the cache and return it's cache position
	int addLocal(string name, double value);
	
	/// Add a local symbol @p name to the cache and return it's cache position
	int addLocal(string name, VDOUBLE value);
	
	/// Read the cache index of local variable name
	int getLocalIdx(const string& name);
	
	
	/// Set a cache local symbol @p name to @p value
	void setLocal(const string& name, double value) {
		locals_cache[getLocalIdx(name)] = value;
	}
	
	/// Set a cache local symbol at position @p cache_pos to @p value
	void setLocal(uint cache_pos, double value) noexcept {
		locals_cache[cache_pos] = value;
	}
	
	/// Set a cache local symbol @p name to @p value
	void setLocal(const string& name, VDOUBLE value) {
		setLocal( getLocalIdx(name), value);
	}
	
	/// Set a cache local symbol at position @p cache_pos to @p value
	void setLocal(uint cache_pos, VDOUBLE value) noexcept {
		locals_cache[cache_pos] = value.x;
		locals_cache[cache_pos+1] = value.y;
		locals_cache[cache_pos+2] = value.z;
	}
	
	/*! \brief Add a foreign Scope @p scope as namespace @p ns_name to the local variable scope.
	 * 
	 * Returns the name space reference @return id. */
	uint addNameSpaceScope(const string& ns_name, const Scope* scope);
	/// Get all symbols used from name space @p ns_id. The namespace prefix is not contained in the symbols returned.
	set<Symbol> getNameSpaceUsedSymbols(uint ns_id) const;
	/// Set the focus of name space @p ns_id
	void setNameSpaceFocus(uint ns_id, const SymbolFocus& f) const;
	
	
	/// Get the memory layout of local symbols. Using that table, setLocals will update all local data at once.
	const vector<LocalSymbolDesc>& getLocalsTable() const { return locals_table; }
	
	/// Set the table of local symbols in a compact way.
	void setLocalsTable(const vector<LocalSymbolDesc>& layout);
	/** Set all local data at once using @p data
	 *  
	 * Update the scalar expansion wrappers must be done manually by calling setExpansionIndex
	 */
	void setLocals(const double *data) noexcept {
// 		if (sizeof(data) != sizeof(double) * locals_cache.size() ) cout << sizeof(data) << " != " << locals_cache.size() <<  " * " << sizeof(double) << endl;
// 		assert(sizeof(data) == sizeof(double) * locals_cache.size() );
		auto it_cache = locals_cache.begin(); auto it_data = data;
		for (;it_cache != locals_cache.end(); it_data++, it_cache++) {
			*it_cache = *it_data;
		}
	}
	
	void setKernelMode(bool enabled=true) { eval_spec.kernel_mode = enabled; } 
	
	void addParserLocal(const string& name) { parser_symbols.insert(name); }
	
	/// Get the current value of symbol @p name
	double get(const string& name); 
	/// Get the current value of symbol at @p idx
	inline double getLocalD(uint idx) { return locals_cache[idx]; }; 
	/// Get the current value of symbol at @p idx
	inline VDOUBLE getLocalV(uint idx) { return VDOUBLE(locals_cache[idx],locals_cache[idx+1],locals_cache[idx+2]); }; 
	
	/// Struct describing the used external symbols (i.e. dependencies) and wether scalar expansion was used to resolve the symbols.
	struct ParserDesc { set<Symbol> ext_symbols; set<string> loc_symbols; bool requires_expansion; };
	
	/// Attach a parser to the cache.
	void attach(mu::Parser *parser);
	/// Detach a parser from the cache.
	void detach(mu::Parser *parser);
	/// Returns a dependency descriptor of the parser's expression
	EvaluatorCache::ParserDesc getDescriptor(mu::Parser *parser);
	
	
	/// Fill the cache with data wrt. @p focus
	void fetch(const SymbolFocus& focus, const bool safe=false);
	
	/// A list of used external symbols.
	std::set<Symbol> getExternalSymbols();
	
	/// Set current index @p idx for the scalar vector expansion (@p idx < 3)
	void setExpansionIndex(uint idx) noexcept;
	
	/// Allow to perform scalar expansion
	void permitScalarExpansion(bool allow) noexcept { scalar_expansion_permitted = allow; }
	
	/// Scalar vector expansion in use
	bool getPartialSpec() noexcept { return eval_spec.allow_partial; };
	
	/// Get a string of all values in the cache
	string toString();
	
protected:
	struct SymbolDesc {
		enum Type { DOUBLE, VECTOR } type;
		enum Source { LOCAL=0, EXTERNAL=1, NS=2, CONSTANT=3 } source;
		bool on_surface = false;
		int cache_idx;
		string name;
		Symbol sym;
	};

	struct ExpansionDesc {
		SymbolDesc::Source source;
		uint cache_source_idx;
		uint cache_dest_idx;
	};

	/** @brief Symbol namespace referring to a scope
	 * 
	 * Namespaces may provide access to other scopes than the expressions main scope. 
	 * Use \ref setFocus() to set the current \ref SymbolFocus of the namespace.
	 * Variable registration is done on demand, so that namespace may not be used at all ...
	 */ 
	class NS {
		private:
			const Scope* scope;
			string ns_name;
			vector<SymbolDesc> used_symbols;
			vector<double>& value_cache;
			
			friend EvaluatorCache;
			
		public:
			NS(std::string name, const Scope* scope, std::vector< double >& value_cache);
			NS(const NS& other, std::vector< double >& value_cache) : value_cache(value_cache) {
				scope = other.scope; ns_name = other.ns_name; used_symbols = other.used_symbols;
			}
			const Scope* getScope() const { return scope; };
			const string& getNameSpace() const { return ns_name; };
			/// Set the current focus of the namespace scope
			void setFocus(const SymbolFocus& f) const;
			
			set<Symbol> getUsedSymbols() const;
			void attach(const std::map<string,int>& index_map, std::vector<double>* value_cache);
	};
	
	/// internal implementation of the symbol factory 
	mu::value_type* registerSymbol_internal(const mu::char_type* symbol);
	
	const Scope* local_scope;
	bool finalized = false;
	// bool allow_partial_spec;
	EvaluatorCacheDesc eval_spec;
	double no_val = std::nan("");
	
	vector<mu::Parser *> attached_parsers;
	map<string, SymbolDesc> cache_symbols;
	vector<double> value_cache;
	map<string, double> constants;
	
	// Local variable storage
	vector<LocalSymbolDesc> locals_table;
	vector<double> locals_cache;
	set<string> parser_symbols;
	
	// External variable storage
	vector<SymbolDesc> flat_externals;
	vector< NS > external_namespaces;
	
	// infrastructure for vector symbol expansion
	bool scalar_expansion_permitted = false;
	bool scalar_expansion = false;
	map<string, ExpansionDesc> expansions;
	
	void finalize();  // create the final data structure and implant all constants.
};


#endif
