//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef Lattice_Data_Layer_H
#define Lattice_Data_Layer_H

#include "lattice.h"
#include "xtensor/xtensor.hpp"
#include "xtensor/xview.hpp"


// struct span {
// 	span(uint a, uint b) : low(a), up(b), length(up-low+1) {};
// 	uint low,up, length;
// 	span operator +(uint b) { return span(low+b,up+b);}
// 	span operator -(uint b) { return span(low-b,up-b);}
// };


template <class T, int shadow_base_width >
class Lattice_Data_Layer {
	
public:
	typedef T value_type;
	typedef T ValType;
	using value_tensor = xt::xtensor<value_type, 3, xt::layout_type::row_major>;
	using boundary_tensor = xt::xtensor<Boundary::Type, 3, xt::layout_type::row_major>;
	using class_type = Lattice_Data_Layer<T,shadow_base_width>;
	
	class ValueReader {
		public:
			virtual ~ValueReader() {};
			virtual void set(string string_val) =0;
			virtual T get(const VINT& pos) const =0;
			virtual bool isSpaceConst() const =0;
			virtual bool isTimeConst() const =0;
			virtual shared_ptr<ValueReader> clone() const =0;
	};
	
	class DefaultValueReader : public ValueReader{
		public:
// 			DefaultValueReader() {};
			DefaultValueReader(T val) : value(val) {};
			void set(string string_val) override { assert(0); }
			T get(const VINT& pos) const override { return value; }
			bool isSpaceConst() const override { return true; }
			bool isTimeConst() const override { return true; }
			shared_ptr<ValueReader> clone() const override { return make_shared<DefaultValueReader>(*this); }
		private:
			T value;
	};
	
	class MapInsideBoundaryValue : public ValueReader {
		public: 
			MapInsideBoundaryValue(class_type* layer) : layer(layer) {};
			void set(string /*string_val*/) override {};
			bool isSpaceConst() const override { return false; }
			bool isTimeConst() const override { return false; };
			T get(const VINT& pos) const override { return layer->get(clamp(pos,VINT(0,0,0),layer->size()-1)); };
			shared_ptr<ValueReader> clone() const override { return make_shared<MapInsideBoundaryValue>(layer); }
		private:
			class_type* layer;
	};
	
	Lattice_Data_Layer(shared_ptr<const Lattice> l, T def_val, string layer_name  = "");

	XMLNode saveToXML() const;
	void loadFromXML(const XMLNode xNode, shared_ptr<ValueReader> converter = make_shared<DefaultValueReader>() );
	void storeData(ostream& out) const;
	bool restoreData(istream & in,  T (* converter)(istream&));
	shared_ptr<const Lattice> getLattice() const { return _lattice; };
	const Lattice& lattice() const { return *_lattice; };

	typename TypeInfo<T>::Return get(VINT a) const;
	bool set(VINT a, typename TypeInfo<T>::Parameter b);
	bool set(const class_type& other) { if (data.shape() ==other.data.shape()) { data = other.data; return true;} else { assert(shadow_size_size_xyz==other.shadow_size_size_xyz); return false;} };
	string getName() const { return name; }
	VINT getWritableSize();
// 	vector<const T*>  getBlock(VINT reference, vector<VINT> offsets) const;
	bool accessible(const VINT& a) const;
	bool writable(const VINT& a) const;
	bool resolve(VINT& a) const { return _lattice->resolve(a); }
	bool writable_resolve(VINT& a) const;
	bool writable_resolve(VINT& a, Boundary::Type& b) const;
// 	DEPRECATED T& get_writable(VINT a);
	auto getData() const {
		return xt::view( data, xt::range(origin.z, top.z), xt::range(origin.y, top.y), xt::range(origin.x, top.x)); 
	}
	Boundary::Type getBoundaryType(Boundary::Codes code) const;
	void set_boundary_value(Boundary::Codes code, value_type a) { boundary_values[code] = make_shared<DefaultValueReader>(a); };
	const VINT& size() const  {return l_size;};
// 	const VINT& size_shadow() const  {return shadow_size;};
	vector<VINT> optimizeNeighborhood(const vector<VINT>& ) const;

	void useBuffer(bool);
	bool setBuffer(const VINT& pos, typename TypeInfo<T>::Parameter val);
	typename TypeInfo<T>::Return getBuffer(const VINT& pos) const;
	void applyBuffer(const VINT& pos);
	void copyDataToBuffer();
	void swapBuffer();
	
	typename TypeInfo<T>::Return unsafe_get(VINT a) const  { a+=origin; return data(a.z,a.y,a.x); }
	typename TypeInfo<T>::Reference unsafe_getRef(VINT a) { a+=origin; return data(a.z,a.y,a.x); }
	typename TypeInfo<T>::ConstReference unsafe_getRef(VINT a) const { a+=origin; return data(a.z,a.y,a.x); }
	void unsafe_set(VINT a, typename TypeInfo<T>::Parameter b) { a+=origin; data(a.z,a.y,a.x) = b; }
	void unsafe_setBuffer(VINT a, typename TypeInfo<T>::Parameter b) { a+=origin; write_buffer(a.z,a.y,a.x) = b; }

protected:
	string name;
	XMLNode stored_node;
	value_type default_value, default_boundary_value, shit_value;
	
	value_tensor data;
	bool using_buffer;
	value_tensor write_buffer;
	value_tensor flux_boundary_data;
	
	bool using_domain;
	boundary_tensor domain;

	shared_ptr<const Lattice> _lattice;
	uint dimensions;
	Lattice::Structure structure;
	VINT l_size;
	VINT shadow_size;  // size of the data grid including shadows
	
	VINT s_origin = {0,0,0}; /// origin of the shadow region 
	VINT origin;          /// origin of the lattice
	VINT top;             /// top of the lattice
	VINT s_top;           /// top of the shadow region
	
	uint shadow_size_size_xyz;  // number of data pints in the grid
	VINT shadow_offset; /// index steps when moving in x,y or z direction 
	VINT shadow_width;  /// shift between the lattice origin and the origin of the shadowed grid
	
	bool has_reduction;
	Boundary::Codes reduction;
	
	uint get_data_index(const VINT& a) const { return dot((a + shadow_width), shadow_offset); }
	static bool less_memory_position(const VINT& a, const VINT& b);
	void allocate();
	
	vector<Boundary::Type> boundary_types;
	vector< shared_ptr<ValueReader> > boundary_values;
	vector< shared_ptr<ValueReader> > boundary_flux_values;
	void setDomain();
	void reset_boundaries();
	
	friend class Domain;
	// friend class EdgeListTracker;
	friend class LatticeStencil;
	friend class MembraneMapper;
	friend class MembraneProperty;
};

#include "lattice_data_layer.cpp"

#endif

