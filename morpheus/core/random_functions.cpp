#include "random_functions.h"
#include <boost/math/distributions/inverse_gamma.hpp>
// make a unique source of randomness available to everyone
// vector<mt19937_64> random_engines;
vector<unsigned int> random_seeds = {0};
thread_local mt19937_64 random_engine;

typedef std::normal_distribution<double> RNG_GaussDist;
typedef std::gamma_distribution<double> RNG_GammaDist;

bool getRandomBool() {
// 	return getRandom01() > 0.5;
	return random_engine() & 1;
}

double getRandom01() {
	static thread_local uniform_real_distribution <double> rnd(0.0,1.0);
	return rnd(random_engine);
}

// random gaussian distribution of stddev s
double getRandomGauss(double s) {
	RNG_GaussDist rnd( 0.0, s);
	return rnd(random_engine);
}

double getRandomGamma(double shape, double scale) {
	RNG_GammaDist rnd( shape );
	return scale*rnd(random_engine);
}

double getRandomInvGamma(double shape, double scale) {
	boost::math::inverse_gamma_distribution<> dist(shape, scale);
	return boost::math:: quantile(dist,getRandom01());
}

unsigned int getRandomUint(uint max_val) {
	uniform_int_distribution<uint> rnd(0,max_val);
    return rnd(random_engine);
}

unsigned long long getRandomLong(unsigned long long max_val) {
	uniform_int_distribution<unsigned long long> rnd(0,max_val);
    return rnd(random_engine);
}

void setRandomSeed(uint random_seed)
{
	// initialize multiple random engines (one for each thread) and set seed
	// 1. make vector of random engines
	// 2. set random seed of first engine taken from XML
// 	cout << "Random seed of master thread = " << random_seed << endl;
	random_engine.seed(random_seed);

	// 3. generate random seeds for other engines using the first engine.
	// 4. set random seed of other engines (for other threads)
	uint numthreads = TP::teamSize();
	random_seeds.resize(numthreads,0);
	random_seeds[0] = random_seed;
	for (int i=0; i<random_seeds.size(); i++) {
		if (i>0) random_seeds[i] = random_engine();
// 		cout << "Random seed of thread " << i << " = " << random_seeds[i] << endl;
	}
}

void initSeed()
{
	unsigned int seed_id = TP::teamNum();
	if (seed_id>random_seeds.size()-1) seed_id = 0;
	cout << "Intitalize Thread " << seed_id << " with seed " << random_seeds[seed_id] << endl;
	random_engine.seed(random_seeds[seed_id]);
}


