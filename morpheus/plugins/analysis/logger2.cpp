#include "logger2.h"
#include "core/membrane_property.h"
int Logger::instances=0;

REGISTER_PLUGIN(Logger);
	
Logger::Logger() : AnalysisPlugin() {
	Logger::instances++; 
    instance_id = Logger::instances;
};
Logger::~Logger(){
	Logger::instances--;
};

void Logger::loadFromXML(const XMLNode xNode, Scope* scope){
	
	slice = false;
	// Load symbols
	XMLNode xInput = xNode.getChildNode("Input");
	inputs.resize(xInput.nChildNode("Symbol"));
	for (uint i=0; i<xInput.nChildNode("Symbol"); i++) {
		inputs[i]->setXMLPath("Input/Symbol["+to_str(i)+"]/symbol-ref");
		// Preliminary allow partially specified symbols
		inputs[i]->setPartialSpecDefault(0);
		registerPluginParameter(inputs[i]);
	}
	
	// Restriction
	// Slice
	map<string, FocusRangeAxis> slice_axis_map;
	slice_axis_map["x"] = FocusRangeAxis::X;
	slice_axis_map["y"] = FocusRangeAxis::Y;
	slice_axis_map["z"] = FocusRangeAxis::Z;
	slice_value.setXMLPath("Restriction/Slice/value");
	slice_axis.setXMLPath("Restriction/Slice/axis");
	slice_axis.setConversionMap(slice_axis_map);
	registerPluginParameter(slice_value);
	registerPluginParameter(slice_axis);
	
	// Celltype
	celltype.setXMLPath("Restriction/Celltype/celltype");
	registerPluginParameter(celltype);
	
	// Cell IDs
	cellids_str.setXMLPath("Restriction/Cells/cell-ids");
	registerPluginParameter(cellids_str);
	
	// Domain
	domain_only.setXMLPath("Restriction/domain-only");
	domain_only.setDefault("false");
	registerPluginParameter(domain_only);
	
	//Condition
	restriction_condition.setXMLPath("Restriction/condition");
	registerPluginParameter(restriction_condition);
	
	// Force node granularity
	//  By default, the granularity is determined automatically by checking the symbol with the smallest granularity
	//  This can be overridden by specifying force-node-granularity="true"
	force_node_granularity.setXMLPath("Input/force-node-granularity");
	force_node_granularity.setDefault("false");
	registerPluginParameter(force_node_granularity);
	// Enforce a spatial data entry for the boundary state on node-wise iteration
	include_boundary.setXMLPath("Input/include-boundary");
	include_boundary.setDefault("false");
	registerPluginParameter(include_boundary);
	// Exclude Medium types
	exclude_medium.setXMLPath("Restriction/exclude-medium");
	registerPluginParameter(exclude_medium);
	
	// output
	XMLNode xOutput = xNode.getChildNode("Output");
	if (xOutput.nChildNode("TextOutput")) {
// 		cout << "Output/TextOutput" << endl;
		auto output = make_shared<LoggerTextWriter>(*this,"Output/TextOutput");
		writers.push_back(output);
	}
	if (xOutput.nChildNode("HDF5Output")) {
		// not implemented yet
	}

	// plots
	if (Gnuplot::isEnabled()) {
		XMLNode xPlots = xNode.getChildNode("Plots");
		for (uint i=0; i<xPlots.nChildNode("Plot"); i++) {
			shared_ptr<LoggerPlotBase> p( new LoggerLinePlot(*this, string("Plots/Plot[") + to_str(i) + "]") );
			plots.push_back(p);
		}
		for (uint i=0; i<xPlots.nChildNode("SurfacePlot"); i++) {
			shared_ptr<LoggerPlotBase> p( new LoggerMatrixPlot(*this, string("Plots/SurfacePlot[") + to_str(i) + "]") );
			plots.push_back(p);
		}
	}
	
	AnalysisPlugin::loadFromXML(xNode,scope);
	
}


void Logger::init(const Scope* scope){
// 	cout << "Logger::init" << endl;

	celltype.init();
	if ( celltype.isDefined() && celltype() ) {
		auto logging_scope = celltype()->getScope();
		restriction_condition.setScope(logging_scope);
		for (auto &c : inputs) {
			c->setScope(logging_scope);
			permit_incomplete_symbols = false;
			c->unsetPartialSpecDefault();
		}
	}
	
   AnalysisPlugin::init(scope);

	try{
		
		// Determine GRANULARITY
		
		// first, set global granularity by default
		logger_granularity = Granularity::Global;
		try {
			for(auto &c : inputs){
				logger_granularity+= c->granularity();
			}
		}
		catch (string s) {
			throw MorpheusException("Incompatible data granularity in Logger: " + s +".", stored_node);
		}

		// if node granularity is forced, set to node granularity
		if( force_node_granularity() == true ){
			logger_granularity = Granularity::Node;
// 			cout << "Logger: Forced node granularity" <<  endl;
		}
      
		// process scope restrictions
		if (cellids_str.isDefined()) {
			// We probably do not require the symbol to be globally defined. 
			// However, it's hard to ensure that the cells will the symbol defined throughout the simulation. 
			// TODO What happens in case the symbol is not defined ? --> crash
			permit_incomplete_symbols = true;
		}
		else if ( celltype.isDefined() && celltype() ) {
			auto logging_scope = celltype()->getScope();
			for (auto &c : inputs) {
				c->setScope(logging_scope);
				permit_incomplete_symbols = false;
				c->unsetPartialSpecDefault();
			}
		}
		else if ( exclude_medium.isDefined() && exclude_medium()) {
			permit_incomplete_symbols = true;
		}
		else if ( ! exclude_medium.isDefined() && (logger_granularity == Granularity::Cell || logger_granularity == Granularity::MembraneNode) ) {
			permit_incomplete_symbols = true;
		}
		else {
			permit_incomplete_symbols = false;
			for (auto &c : inputs) {
				c->unsetPartialSpecDefault();
			}
		}
		
		if (slice_axis.isDefined() && slice_value.isDefined()) {
			slice = true;
		}
// 		cout << "Slice...?" << (slice?"true":"false") << endl;
		
		// Collect RESTRICTIONS
		
		if( cellids_str.isDefined() ){
			cellids = parseCellIDs( cellids_str() );
			for(auto &i : cellids){
				if( CPM::cellExists(i) ){
					restrictions.insert( {FocusRangeAxis::CELL, i} );
				}
			}
		}
		else if( celltype.isDefined() ){
			if(celltype()) {
				restrictions.insert( {FocusRangeAxis::CellType, celltype()->getID()} );
			}
			else
				throw MorpheusException(string("Cannot restrict Logger to celltype "+celltype()->getName()+" because it is not defined."), stored_node);
		}
		else if ( (exclude_medium.isDefined() && exclude_medium()) || (!exclude_medium.isDefined() && (logger_granularity == Granularity::Cell || logger_granularity == Granularity::MembraneNode) )) {
			// The FocusRange excludes CellTypes depending on celltype->isMedium()
			// We just have to keep the partial spec option for the symbols.
			// Right now there is no option to include the medium types
			restrictions = FocusRange::getBiologicalCellTypesRestriction();
		}
		
		if( slice ){
			if( logger_granularity != Granularity::Node )
                throw MorpheusException("Restriction to slice is only available for node granularity (e.g. Fields).\nYou may want to use the force-node-granularity option.", stored_node);
			restrictions.insert( pair<FocusRangeAxis, int>( slice_axis(),int( slice_value.get(SymbolFocus()))) );
		}

		// Now, we can set the FocusRange of the Logger
		range = FocusRange(logger_granularity, restrictions, domain_only());

// 		cout << "Logger INIT:  " << endl;
// 		cout << "Granularity:  " << logger_granularity <<  endl;
// 		cout << "Restrictions: " <<  endl;
// 		for(auto &r : restrictions)
// 			cout << int(r.first) << " = " << int(r.second) << endl;
// 		cout << "Range:        " << range.size() << endl;

		// initialize output
		for (auto o : writers){
			o->init();
		}
		
		// initialize plots
		for (auto p : plots){
			p->init();
		}
	}
	catch (string e) {
		throw MorpheusException(e,this->stored_node);
	}
	catch (GnuplotException e) {
		throw MorpheusException(e.what(), this->stored_node);
    }
};

const SymbolAccessor<double> Logger::getInput(const string& symbol) const {
	if (! writers.empty() && dynamic_pointer_cast<LoggerTextWriter>(writers.front()) ) {
		const auto& writer = dynamic_pointer_cast<LoggerTextWriter>(writers.front());
		if (writer->getSymbol(symbol)) {
			return writer->getSymbol(symbol);
		}
	}
	return {};
}

const SymbolAccessor<double> LoggerTextWriter::getSymbol(const string& symbol) const { 
	for (const auto& o : outputs) {
		if (o.symbol->name() == symbol) return o.symbol;
	}
	return {};
};

int LoggerTextWriter::getSymbolColumn(const string& symbol) const {
	
	for (int i=0; i<outputs.size(); i++) {
		if (outputs[i].symbol->name() == symbol) return i;
	}
	return -1;
};

string  LoggerTextWriter::getSymbolDescription(const string& name) const {
	for (const auto& o : outputs) {
		if (o.symbol->name() == name) {
			return Gnuplot::sanitize(o.symbol->description().empty() ? o.symbol->name() : o.symbol->description());
		}
	}
	return Gnuplot::sanitize(name);
}

std::pair< double, double > LoggerTextWriter::getSymbolRange(const std::string& name) const
{
	pair<double,double> range {0,0};
	for (const auto& o : outputs) {
		if (o.symbol->name() == name && o.min != std::numeric_limits<double>::max() ) {
			range.first = o.min;
			range.second = o.max;
		}
	}
	return range;
}

int Logger::addWriter(shared_ptr<LoggerWriterBase> writer)
{
// 	cout << "Adding another Writer to the Logger" << endl;
	writers.push_back(writer);
	return writers.size()-1;
}

void Logger::analyse(double time){
	//cout << "Logger::analyse: " << time << "\t" << SIM::getTime() << endl;
	// write output files
	for (auto out : writers) {
		out->write();
	}
	//  and draw plots
	for(auto p : plots) {
		p->checkedPlot();
	}
}

void Logger::finish(){
//    cout << "Logger::finish..." << endl;
// 	for(auto p:plots)
// 		p->finish();
}

vector<CPM::CELL_ID> Logger::parseCellIDs(string cell_ids_string){
// 	cout << "Logger::parseCellIDs" << endl;
	vector<CPM::CELL_ID> ids;
	vector<string> tokens;
	tokens = tokenize(cell_ids_string, ",");
	for(uint i=0; i < tokens.size(); i++){
		cout << tokens[i] << "\n";
		vector<string> cell_ids = tokenize(tokens[i], "-");
		if(cell_ids.size() == 2){
			uint s = atoi(cell_ids[0].c_str());
			uint e = atoi(cell_ids[1].c_str());
			for(uint j = s; j <= e; j++){
				cout << j << ", ";
				ids.push_back( j );
			}
			cout << "\n";
		}
		else
			ids.push_back( atoi(tokens[i].c_str()) );
	}
	if ( ids.empty() ){
		throw string("No cell IDs were extracted from provided string: " + cell_ids_string);
	}

	return ids;
}

LoggerTextWriter::LoggerTextWriter(Logger& logger, string xml_base_path) : LoggerWriterBase(logger)
{ 
	output_scope=nullptr;
	
	header.setXMLPath(xml_base_path+"/header");
	header.setDefault("true");
	logger.registerPluginParameter(header);
	
	header_guarding.setXMLPath(xml_base_path+"/header-guarding");
	header_guarding.setDefault("true");
	logger.registerPluginParameter(header_guarding);
	
	map<string, string> separator_convmap;
	separator_convmap["tab"]="\t";
	separator_convmap["comma"]=",";
	separator_convmap["semicolon"]=";";
	separator.setConversionMap(separator_convmap);
	separator.setXMLPath(xml_base_path+"/separator");
	separator.setDefault("tab");
	logger.registerPluginParameter(separator);
	
	filename.setXMLPath(xml_base_path + "/file-name");
	filename.setDefault("logger");
	logger.registerPluginParameter(filename);
	
	map<string, FileNumbering> numbering_map;
	numbering_map["time"] = FileNumbering::TIME;
	numbering_map["sequential"] = FileNumbering::SEQUENTIAL;
	file_numbering.setConversionMap(numbering_map);
	file_numbering.setDefault("time");
	file_numbering.setXMLPath(xml_base_path+"/file-numbering");
	logger.registerPluginParameter(file_numbering);
	
	map<string,FileSeparation> separation_map;
	separation_map["none"] = FileSeparation::NONE ;
	separation_map["time"] = FileSeparation::TIME ;
	separation_map["cell"] = FileSeparation::CELL ;
	separation_map["time+cell"] = FileSeparation::TIME_CELL;
	xml_file_separation.setXMLPath(xml_base_path+"/file-separation");
	xml_file_separation.setConversionMap(separation_map);
	xml_file_separation.setDefault("none");
	logger.registerPluginParameter(xml_file_separation);
	
	
	xml_file_format.setXMLPath(xml_base_path + "/file-format");
	map<string,OutputFormat> format_map;
	format_map["csv"] = OutputFormat::CSV;
	format_map["matrix"] = OutputFormat::MATRIX;
	xml_file_format.setConversionMap(format_map);
	logger.registerPluginParameter(xml_file_format);
	
	forced_format = false;
};

LoggerTextWriter::LoggerTextWriter(Logger& logger, LoggerTextWriter::OutputFormat format): LoggerWriterBase(logger)
{
	output_scope = nullptr;
	header.read("true");
	
	map<string, string> separator_convmap;
	separator_convmap["tab"]="\t";
	separator_convmap["comma"]=",";
	separator_convmap["semicolon"]=";";
	separator.setConversionMap(separator_convmap);
	separator.setDefault("tab");
	separator.read("tab");
	
	header_guarding.setDefault("true");
	header_guarding.read("true");
	
	
	filename.setDefault("logger");
	filename.read("logger");
	
	map<string, FileNumbering> numbering_map;
	numbering_map["time"] = FileNumbering::TIME;
	numbering_map["sequential"] = FileNumbering::SEQUENTIAL;
	file_numbering.setConversionMap(numbering_map);
	file_numbering.setDefault("time");
	file_numbering.read("time");
	
	map<string,FileSeparation> separation_map;
	separation_map["none"] = FileSeparation::NONE ;
	separation_map["time"] = FileSeparation::TIME ;
	separation_map["cell"] = FileSeparation::CELL ;
	separation_map["time+cell"] = FileSeparation::TIME_CELL;
	xml_file_separation.setConversionMap(separation_map);
	xml_file_separation.setDefault("time");
	xml_file_separation.read("cell");
	
	forced_format = true;
	file_format = format;
}


void LoggerTextWriter::init() {

// 	cout << "LoggerTextWriter::init" << endl;

	if ( (filename() == "logger" || filename() == "automatic") ) {
		file_basename = "logger";
		if (logger.getInstanceNum()>1) {
			file_basename+="_"+to_str(logger.getInstanceID());
		}
	}
	else 
		file_basename = filename();
	
	file_extension = ".csv";
	
	file_write_count = 0;
	
	output_granularity = logger.getGranularity();
	FocusRange range(output_granularity, logger.getRestrictions(), logger.getDomainOnly(), logger.getIncludeBoundary() );
	
	// Pick the right scope from logger restrictions definition
	output_scope=SIM::getGlobalScope();
	if (logger.getRestrictions().count(FocusRangeAxis::CellType)==1 )
		output_scope = CPM::getCellTypes()[logger.getRestrictions().find(FocusRangeAxis::CellType)->second].lock()->getScope();
	
	if (forced_format) {
		if (file_format == OutputFormat::MATRIX && !range.isRegular() ) {
			throw string("Logger TextWriter: Cannot produce matrix data from given data (range is irregular).");
		}
	}
	else if(xml_file_format.isDefined()) {
		file_format = xml_file_format();
		if (file_format == OutputFormat::MATRIX && !range.isRegular() ) {
			throw string("Logger TextWriter: Cannot produce matrix data from given data (range is irregular).");
		}
	}
	else {
		if ( !range.isRegular() || range.dataAxis().size()<1 || (range.dataAxis().size()==1 && range.dataAxis()[0] == FocusRangeAxis::CELL) )
			file_format = OutputFormat::CSV;
		else
			file_format = OutputFormat::MATRIX;
	}
	
	file_separation = xml_file_separation();
	// Check that the file_separation value makes sense
	if (range.dataAxis().empty() || range.dataAxis()[0] != FocusRangeAxis::CELL) {
		if ( file_separation == FileSeparation::CELL)
			file_separation = FileSeparation::NONE;
		if (file_separation == FileSeparation::TIME_CELL)
			file_separation = FileSeparation::TIME;
	}
	
	cout << "Logger range:" << endl;
	cout << "-> size " << range.size() << endl;
	cout << "-> dataAxes "; for (auto a : range.dataAxis()) cout << FocusRange::axisName(a) << ", "; cout << endl;
	cout << "-> data.sizes: "; for (auto &s :range.dataSizes() ) { cout << s << ", "; }; cout << endl;
	cout << "-> file separation: " << (file_separation==FileSeparation::NONE ? "None" : file_separation==FileSeparation::CELL ? "CELL" : file_separation==FileSeparation::TIME ? "TIME" : "TIME+CELL") << endl;
	
	
	matrix_time_increment=0;
	if (file_separation != FileSeparation::TIME_CELL && file_separation != FileSeparation::TIME) {
		matrix_time_increment=range.dataSizes().size()>1;
		for (int i=0; i+2<range.dataSizes().size(); i++ ) {
			int axis_index = i + (range.dataAxis().size()-range.dataSizes().size());
			cout << FocusRange::axisName(range.dataAxis()[axis_index]) << " " << (file_separation == FileSeparation::CELL ? "separating":"non-separating") << endl;
			if (range.dataAxis()[axis_index] == FocusRangeAxis::CELL && file_separation == FileSeparation::CELL ) {
				// if (matrix_time_increment==0) matrix_time_increment=1;
			}
			else {
				matrix_time_increment *= range.dataSizes()[i];
			}
		}
	}
	
	if (file_format == OutputFormat::CSV ) {
		outputs.push_back( { output_scope->findSymbol<double>(SymbolBase::Time_symbol) } );
		
		for (auto& axis :range.dataAxis() ) {
			switch(axis) {
				case FocusRangeAxis::CELL :
					outputs.push_back( output_scope->findSymbol<double>(SymbolBase::CellID_symbol) );
					break;
				case FocusRangeAxis::X :
					outputs.push_back( output_scope->findSymbol<double>(SymbolBase::Space_symbol+".x") );
					break;
				case FocusRangeAxis::Y :
					outputs.push_back( output_scope->findSymbol<double>(SymbolBase::Space_symbol+".y") );
					break;
				case FocusRangeAxis::Z :
					outputs.push_back( output_scope->findSymbol<double>(SymbolBase::Space_symbol+".z") );
					break;
				case FocusRangeAxis::MEM_X :
					outputs.push_back( output_scope->findSymbol<double>(SymbolBase::MembraneSpace_symbol+".phi") );
					break;
				case FocusRangeAxis::MEM_Y :
					outputs.push_back( output_scope->findSymbol<double>(SymbolBase::MembraneSpace_symbol+".theta") );
					break;
				case FocusRangeAxis::NODE :
					outputs.push_back( output_scope->findSymbol<double>(SymbolBase::Space_symbol+".x") );
					if (SIM::lattice().getDimensions()>1) {
						outputs.push_back( output_scope->findSymbol<double>(SymbolBase::Space_symbol+".y") );
					}
					if (SIM::lattice().getDimensions()>2) {
						outputs.push_back( output_scope->findSymbol<double>(SymbolBase::Space_symbol+".z") );
					}
					break;
				default:
					assert(0);
					break;
			}
		}
		
		auto& symbols = logger.getInputs();
		for (auto& s : symbols) {
			outputs.push_back( s->accessor() );
		}
	}
	else if (file_format == OutputFormat::MATRIX) {
		auto& symbols = logger.getInputs();
		for (auto& s : symbols) {
			outputs.push_back( s->accessor() );
		}
	}
	
}

int LoggerTextWriter::addSymbol(const string& name) {
	 assert(output_scope);
// 		output_scope = SIM::getGlobalScope();
	SymbolAccessor<double> d;
	if (logger.permitIncompleteSymbols())
		d=output_scope->findSymbol<double>(name,0);
	else
		d=output_scope->findSymbol<double>(name);
	
	try {
		output_granularity += d->granularity();
	}
	catch(...) {
		throw string ("Incompatible output granularity in Logger/TextOutput for symbol '") + name + "'.";
	}
	
	// if the granularity is changed, we might have to add a the Column for the respective index ...
	
	FocusRange range(output_granularity, logger.getRestrictions(), logger.getDomainOnly(), logger.getIncludeBoundary() );
	if (OutputFormat::CSV == file_format) {
		vector<string> axis_symbol_names;
		for (auto& axis :range.dataAxis() ) {
			switch(axis) {
				case FocusRangeAxis::CELL :
					axis_symbol_names.push_back(SymbolBase::CellID_symbol);
					break;
				case FocusRangeAxis::X :
					axis_symbol_names.push_back(SymbolBase::Space_symbol+".x");
					break;
				case FocusRangeAxis::Y :
					axis_symbol_names.push_back(SymbolBase::Space_symbol+".y");
					break;
				case FocusRangeAxis::Z :
					axis_symbol_names.push_back(SymbolBase::Space_symbol+".z");
					break;
				case FocusRangeAxis::MEM_X :
					axis_symbol_names.push_back(SymbolBase::MembraneSpace_symbol+".phi");
					break;
				case FocusRangeAxis::MEM_Y :
					axis_symbol_names.push_back(SymbolBase::MembraneSpace_symbol+".theta");
					break;
				case FocusRangeAxis::NODE :
					axis_symbol_names.push_back(SymbolBase::Space_symbol+".x");
					if (SIM::lattice().getDimensions()>1) { axis_symbol_names.push_back(SymbolBase::Space_symbol+".y"); }
					if (SIM::lattice().getDimensions()>2) { axis_symbol_names.push_back(SymbolBase::Space_symbol+".z"); }
					break;
				default:
					assert(0);
					break;
			}
		}
		for (const auto& axis_sym : axis_symbol_names) {
			if (std::find_if(outputs.begin(), outputs.end(), [&](const OutputSymbol& val) { return val.symbol->name() == axis_sym; } ) == outputs.end() ) {
				outputs.push_back(output_scope->findSymbol<double>(axis_sym));
			}
			
		}
	}
	// If granularity has changed, we might revise our rejection on file separation
	file_separation = xml_file_separation();
	// Check that the file_separation value makes sense
	if (range.dataAxis().empty() || range.dataAxis()[0] != FocusRangeAxis::CELL) {
		if ( file_separation == FileSeparation::CELL)
			file_separation = FileSeparation::NONE;
		if (file_separation == FileSeparation::TIME_CELL)
			file_separation = FileSeparation::TIME;
	}
	
	outputs.push_back(d);
	logger.registerInputSymbol(d);
	return outputs.size()-1;
}

std::tuple<string,int> LoggerTextWriter::getDataFile(double time, string symbol) const {
	return getDataFile(SymbolFocus::global, time, symbol);
}

std::tuple<string,int> LoggerTextWriter::getDataFile(const SymbolFocus& f, double time, string symbol) const {
	stringstream fn; 
	fn << file_basename;
	
	if (!symbol.empty()) {
		fn << "_"<< symbol;
	}
	
	int matrix_index = 0;
	switch (file_separation) {
		case FileSeparation::NONE :
			// cout << matrix_time_increment << " * " << file_write_count<< endl; 
			matrix_index = matrix_time_increment * (file_write_count -1);
			break;
		case FileSeparation::CELL :
			// cout << matrix_time_increment << " * " << file_write_count<< endl; 
			fn << "_" << f.cellID();
			matrix_index = matrix_time_increment * (file_write_count -1);
			break;
		case FileSeparation::TIME_CELL :
			fn <<  "_" << f.cellID();
		case FileSeparation::TIME :
			if (file_numbering() == FileNumbering::TIME)
				fn << "_" << SIM::getTimeName(time);
			else 
				fn << "_" << setfill('0') << setw(4) << file_write_count;
			break;
	}
	
	fn << file_extension;
	const auto count = file_open_count.find(fn.str());
	matrix_index = (count == file_open_count.end()) ? 0 : count->second * matrix_time_increment;
	return {fn.str(), matrix_index};
}

vector<string> LoggerTextWriter::getDataFiles() {
	vector<string> ret;
	for (const auto& file : file_open_count)  {
		ret.push_back(file.first);
	}
	return ret;
}

std::tuple<unique_ptr<ofstream>, bool> LoggerTextWriter::getOutFile(const SymbolFocus& focus, string symbol) {
	
	auto [filename,matrix_index] = getDataFile(focus, SIM::getTime(), symbol);
	
	// if we already had the file just open it
	auto file_stat = file_open_count.find(filename);
	if (file_stat != file_open_count.end()) {
		auto out = make_unique<ofstream>(filename, ofstream::out | ofstream::app);
		*out  << setprecision(10);
		file_stat->second++;
		return {std::move(out), false};
	}
	
	// first time opened : truncate and write header
	auto out = make_unique<ofstream>(filename, ofstream::out | ofstream::trunc);
	if (file_format == OutputFormat::CSV && header() ) {
		//out << "#";
		string guard =  header_guarding() ? "\"" : "";
		bool first = true;
		for (auto& o : outputs) {
			*out << (first ? "" : separator()) << guard << o.header << guard;
			first = false;
		}
		*out << "\n";
	}
	*out  << setprecision(10);
	if (file_format == OutputFormat::MATRIX && header() ) {
		// write header
		// TODO Matrix header missing ...
	}
	
	file_open_count.insert({filename,0});
	
	return {std::move(out),true};
}


void LoggerTextWriter::write()
{
	file_write_count++;
	
	if (file_format == OutputFormat::CSV)
		writeCSV();
	else
		writeMatrix();
}


void LoggerTextWriter::writeCSV() {
	
	FocusRange range(output_granularity, logger.getRestrictions(), logger.getDomainOnly(), logger.getIncludeBoundary());
	const auto& condition = logger.getRestrictionCondition();
// 	auto& symbols = logger.getInputs();
	
	stringstream time;
	time << SIM::getTime();
	
	bool separate_cells = (file_separation == FileSeparation::TIME_CELL) || ((file_separation == FileSeparation::CELL) && (range.dataAxis()[0] == FocusRangeAxis::CELL));
	if (separate_cells) {
		auto cells = range.cells();
		multimap<FocusRangeAxis,int> plain_restrictions = logger.getRestrictions();
		auto cell_restr = plain_restrictions.equal_range(FocusRangeAxis::CELL);
		plain_restrictions.erase(cell_restr.first, cell_restr.second);
		auto celltype_restr = plain_restrictions.equal_range(FocusRangeAxis::CellType);
		plain_restrictions.erase(celltype_restr.first, celltype_restr.second);
		
		for (auto cell : cells) {
			try {
				SymbolFocus cell_focus(cell);
				if (condition.isDefined() && condition.granularity() <= Granularity::Cell) {
					if (!condition(SymbolFocus(cell))) continue;
				}
			}
			catch (const string & e) {
				continue;
			}
		
			multimap<FocusRangeAxis,int> restrictions = plain_restrictions;
			restrictions.insert(make_pair(FocusRangeAxis::CELL, cell) );
			FocusRange cell_range(output_granularity, restrictions, logger.getDomainOnly());
			auto [out, first_write] = getOutFile( *(cell_range.begin()) );
			try {
				for (const SymbolFocus& focus : cell_range) {
					if (condition.isDefined() && condition.granularity() > Granularity::Cell) {
						if (!condition(focus)) continue;
					}
					// write point of data row
					for (uint i=0; i<outputs.size(); i++ ) {
						if (i!=0) *out << separator();
						auto d = outputs[i].symbol->get( focus );
						if (outputs[i].min > d) outputs[i].min = d;
						if (outputs[i].max < d) outputs[i].max = d;
						*out << d;
					}
					*out << "\n";
				}
			}
			catch (const string& e) {
				*out << "\n";
				continue;
			}
		}
		
	}
	else {
		auto [out, first_write] = getOutFile( *(range.begin()) );
		for (const SymbolFocus& focus : range) {
			try {
				if (condition.isDefined() && !condition(focus)) continue;
				// write point of data row header
				for (uint i=0; i<outputs.size(); i++ ) {
					if (i!=0) *out << separator();
					auto d = outputs[i].symbol->get( focus );
					if (outputs[i].min > d) outputs[i].min = d;
					if (outputs[i].max < d) outputs[i].max = d;
					*out << d;
				}
			}
			catch (const string& e) {
				*out << "\n";
				continue;
			}
			*out << "\n";
		}
		// treat single cell as 0-dimensional data
		int dim = range.dimensions();
		if( dim == 1 && range.size() == 1)
			dim = 0;
		// only separate multi-dimensional data
		if( dim > 0 )
			*out << "\n";

	}
}


void LoggerTextWriter::writeMatrix() {
	// assert(range.isRegular());
	string sep = separator();

	for (auto& o : outputs) {
		auto outer_range = FocusRange(logger.getGranularity(),logger.getRestrictions());
		multimap<FocusRangeAxis,int> inner_restrictions = logger.getRestrictions();
		bool separate_cells = (file_separation == FileSeparation::TIME_CELL || file_separation == FileSeparation::CELL) && (outer_range.dataAxis().size()>0 && outer_range.dataAxis()[0] == FocusRangeAxis::CELL);
		if (separate_cells) {
			auto cell_restr = inner_restrictions.equal_range(FocusRangeAxis::CELL);
			inner_restrictions.erase(cell_restr.first, cell_restr.second);
			auto celltype_restr = inner_restrictions.equal_range(FocusRangeAxis::CellType);
			inner_restrictions.erase(celltype_restr.first, celltype_restr.second);
			outer_range = FocusRange(Granularity::Cell,logger.getRestrictions());
		}
		else {
			outer_range = FocusRange(Granularity::Global,logger.getRestrictions());
		}
			
// #pragma omp parallel for if (outer_range.size()>1)
		for (auto focus : outer_range ) {
			// const auto& cond = logger.getRestrictionCondition();
			// if (cond.isDefined() && cond.granularity() <= Granularity::Cell && ! cond(SymbolFocus(cells[c]))) continue;
			multimap<FocusRangeAxis,int> restrictions = inner_restrictions;
			if (separate_cells)
				restrictions.insert( make_pair(FocusRangeAxis::CELL, focus.cellID()) );
			FocusRange range(logger.getGranularity(), restrictions);
			auto [out, first_write] = getOutFile( focus, o.symbol->name());
				
			bool write_header_block=header();
			int col_count = range.dataSizes().back();
			int row_count = range.dimensions()>1 ? range.dataSizes()[range.dimensions()-2] : 1;
			uint col=0;
			uint row=0;
			for (auto f : range) {
				if (write_header_block ){
					// write header above columns
					// for multiD, write header at the start of each data block
					if ( range.dimensions() > 1 || first_write ){
						writeMatrixColHeader(range, *out);
					}
					write_header_block=false;
				}
				if (col==0 && header() )
					// write header for row
					writeMatrixRowHeader(f, range, *out);
				
				auto d = o.symbol->get(f);
				if (o.min > d) o.min = d;
				if (o.max < d) o.max = d;
				*out << d;
				col++;
				if (col==col_count) {
					*out << "\n";
					col=0; row++;
					if (row == row_count && row_count>1) {
						*out << "\n";
						row=0;
						write_header_block=header();
					}
				}
				else 
					*out << sep;
			}
			// if 2D surface (e.g. membraneprop), separate the data sets by double blank line (to use gnuplot's index keyword)
			if( range.dimensions() > 1 )
				*out << "\n\n";
		}
	}
}

void LoggerTextWriter::writeMatrixColHeader(FocusRange range, ofstream& fs)
{
	// write a header line containing
	// - first: number of columns
	// - then: x coordinates of matrix
	vector<int> sizes = range.dataSizes();
	string sep = separator();
	int dimensions = range.dimensions();
	Granularity granularity = logger.getGranularity();
	bool write_coordinate = true;
	int numcolumns = sizes[ sizes.size() - 1 ];
	fs << numcolumns << sep;
	int colnum = 1;
	double x_value = 0.0;
	double x_min = numeric_limits<double>::max();
	double y_min = numeric_limits<double>::max();
	double x_max = numeric_limits<double>::min();
	double y_max = numeric_limits<double>::min();
	for(SymbolFocus focus : range){
		switch( granularity ){
			case Granularity::Global:{
				// senseless to write Global to Matrix
				break;
			}
			case Granularity::Cell:{
				x_value = focus.cellID();
				//axes.x.label = SymbolData::CellID_symbol;
				break;
			}
			case Granularity::MembraneNode:{
				x_value = 2*M_PI*focus.membrane_pos().x/MembraneProperty::getResolution();
				//axes.x.label = SymbolData::MembraneSpace_symbol;
				break;
			}
			case Granularity::Node:{
				
				auto restrictions = logger.getRestrictions();
				// check whether there is a slice-restriction
				bool is_slice = false;
				FocusRangeAxis slice;
				for(auto restriction : restrictions){
					if( restriction.first == FocusRangeAxis::X ||
						restriction.first == FocusRangeAxis::Y ||
						restriction.first == FocusRangeAxis::Z ){
						is_slice = true;
						slice = restriction.first;
					}
				} 
				
				if( is_slice ){
					switch( slice ){
						case FocusRangeAxis::X:{
							// y is cols, z is rows
							x_value = focus.pos().y;
							//axes.x.label = SymbolData::Space_symbol+".y";
							break;
						}
						case FocusRangeAxis::Y:{
							// x is cols, z is rows
							x_value = focus.pos().x;
							//axes.x.label = SymbolData::Space_symbol+".x";
							break;
						}
						case FocusRangeAxis::Z:{
							// x is cols, y is rows
							x_value = focus.pos().x;
							//axes.x.label = SymbolData::Space_symbol+".x";
							break;
						}
					}
				}
				else{
					x_value = focus.pos().x;
					//axes.x.label = SymbolData::Space_symbol+".x";
				}
				break;
			}
		}
		if( x_value < x_min )
			x_min = x_value;
		if( x_value > x_max )
			x_max = x_value;
		
		if (colnum>1) fs << sep;
		fs << x_value;
		colnum++;
		if( colnum > numcolumns )
			break;
	}
	fs << "\n";
	fs.flush();	
}

void LoggerTextWriter::writeMatrixRowHeader(SymbolFocus focus, FocusRange range, ofstream& fs){
	Granularity granularity = logger.getGranularity();
	double y_value = 0.0;
	double x_min = numeric_limits<double>::max();
	double y_min = numeric_limits<double>::max();
	double x_max = numeric_limits<double>::min();
	double y_max = numeric_limits<double>::min();

	switch( granularity ){
		case Granularity::Global:
			// senseless to write Global to Matrix
			break;
		case Granularity::Cell:{
			y_value = SIM::getTime();
			break;
		}
		case Granularity::MembraneNode:{
			if( range.dimensions() > 1 ){
				y_value = 2*M_PI*(double(focus.membrane_pos().y)/MembraneProperty::getResolution()-0.25);
			}
			else{
				y_value = SIM::getTime();
			}
				
			break;
		}
		case Granularity::Node:{
			if( range.dimensions() == 1 ){
				y_value = SIM::getTime();
			}
			else{ // dimension > 1
				auto restrictions = logger.getRestrictions();
				// check whether there is a slice-restriction
				bool is_slice = false;
				FocusRangeAxis slice;
				for(auto restriction : restrictions){
					if( restriction.first == FocusRangeAxis::X ||
						restriction.first == FocusRangeAxis::Y ||
						restriction.first == FocusRangeAxis::Z ){
						is_slice = true;
						slice = restriction.first;
					}
				} 
				
				if( is_slice ){
					switch( slice ){
						case FocusRangeAxis::X:{
							// y is cols, z is rowsseparator
							y_value = focus.pos().z;
							break;
						}
						case FocusRangeAxis::Y:{
							// x is cols, z is rows
							y_value = focus.pos().z;
							break;
						}
						case FocusRangeAxis::Z:{
							// x is cols, y is rows
							y_value = focus.pos().y;
							break;
						}
					}
				}
				else {
					y_value = focus.pos().y;
				}
			}
			break;
		}
	}
	if( y_value < y_min )
		y_min = y_value;
	if( y_value > y_max )
		y_max = y_value;
	fs << y_value << separator();
}

//// -------------------------------------------------------------------------------------------


LoggerPlotBase::LoggerPlotBase(Logger& logger, string xml_base_path) : logger(logger)
{
	map<string, Terminal> terminalmap;
	terminalmap["png"] = Terminal::PNG;
	terminalmap["pdf"] = Terminal::PDF;
	terminalmap["jpeg"] = Terminal::JPG;
	terminalmap["gif"] = Terminal::GIF;
	terminalmap["svg"] = Terminal::SVG;
	terminalmap["postscript"] = Terminal::EPS;
	terminalmap["screen"] = Terminal::SCREEN;
	
	time_step.setXMLPath(xml_base_path+"/time-step");
	logger.registerPluginParameter(time_step);
	
	
	title.setXMLPath(xml_base_path+"/title");
	logger.registerPluginParameter(title);

	logcommands.setXMLPath(xml_base_path+"/log-commands");
	logcommands.setDefault( "false" );
	logger.registerPluginParameter(logcommands);
	
	terminal.setXMLPath(xml_base_path+"/Terminal/terminal");
	terminal.setConversionMap(terminalmap);
	terminal.setDefault( "png" );
	logger.registerPluginParameter(terminal);
	
	terminal_file_extension[Terminal::PNG] = "png";
	terminal_file_extension[Terminal::PDF] = "pdf";
	terminal_file_extension[Terminal::JPG] = "jpg";
	terminal_file_extension[Terminal::SVG] = "svg";
	terminal_file_extension[Terminal::EPS] = "eps";
	terminal_file_extension[Terminal::GIF] = "gif";
	terminal_file_extension[Terminal::SCREEN] = "";
	
	terminal_name[Terminal::PNG] = "pngcairo";
	terminal_name[Terminal::PDF] = "pdfcairo";
	terminal_name[Terminal::JPG] = "jpeg";
	terminal_name[Terminal::SVG] = "svg";
	terminal_name[Terminal::EPS] = "epscairo";
	terminal_name[Terminal::GIF] = "gif";
	
	plotsize.setXMLPath(xml_base_path+"/Terminal/plot-size");
    logger.registerPluginParameter(plotsize);

	
	map<string, FileNumbering> file_numbering_map;
	file_numbering_map["time"] = FileNumbering::TIME;
	file_numbering_map["sequential"] = FileNumbering::SEQUENTIAL;
	
    file_numbering.setXMLPath(xml_base_path+"/file-numbering");
	file_numbering.setConversionMap(file_numbering_map);
    file_numbering.setDefault( "time" );
    logger.registerPluginParameter(file_numbering);
	
	last_plot_time = 0.0;
	plot_num = 0; // for sequential file numbering
}

void LoggerPlotBase::init() {
	// Check gnuplot has cairo available
	auto gnu_terminals = Gnuplot::get_terminals();
	if (gnu_terminals.count("pngcairo")==0 ) {
		terminal_name[Terminal::PNG] = "png";
	}
	if (gnu_terminals.count("pdfcairo")==0 ) {
		terminal_name[Terminal::PDF] = "pdf";
	}
	if (gnu_terminals.count("epscairo")==0 ) {
		terminal_name[Terminal::EPS] = "postscript";
	}
}

void LoggerPlotBase::checkedPlot()
{
// 	cout << SIM::getTime() << "\t" << (last_plot_time + time_step()) << endl;
	double ts = logger.timeStep() * 10.0;
	if ( time_step.isDefined() ) {
		ts = time_step();
	}
	
	bool is_start = SIM::getTime() - SIM::getStartTime() < 10e-6;
	bool is_stop  = SIM::getStopTime() -  SIM::getTime() < 10e-6;
	if (ts <= 0.0) {
		if (is_stop) {
			this->plot();
			last_plot_time = SIM::getTime();
		}
	}
	else if( (last_plot_time + ts) - SIM::getTime() < 10e-6   // t > t_-1 + dt 
		|| is_start || is_stop ) {
		this->plot(); last_plot_time = SIM::getTime();
	}
}



LoggerLinePlot::LoggerLinePlot(Logger& logger, string xml_base_path) : LoggerPlotBase(logger, xml_base_path)
{
	   // --- Range ---
	map<string, TimeRange> timerangemap;
	timerangemap["all"] = TimeRange::ALL;
	timerangemap["since last plot"] = TimeRange::SINCELAST;
	timerangemap["history"] = TimeRange::HISTORY;
	timerangemap["current"] = TimeRange::CURRENT;

	timerange.setXMLPath(xml_base_path+"/Range/Time/mode");
	timerange.setConversionMap(timerangemap);
	timerange.setDefault( "all" );
	logger.registerPluginParameter(timerange);

	history.setXMLPath(xml_base_path+"/Range/Time/history");
	logger.registerPluginParameter(history);

	first_line.setXMLPath(xml_base_path+"/Range/Data/first-line");
	logger.registerPluginParameter(first_line);

	last_line.setXMLPath(xml_base_path+"/Range/Data/last-line");
	logger.registerPluginParameter(last_line);

	increment.setXMLPath(xml_base_path+"/Range/Data/increment");
	logger.registerPluginParameter(increment);
	
	    // --- Style ---

	map<string, Style> stylemap;
	stylemap["points"]         = Style::POINTS;
	stylemap["lines"]          = Style::LINES;
	stylemap["linespoints"]    = Style::LINESPOINTS;

	style.setXMLPath(xml_base_path+"/Style/style");
	style.setConversionMap(stylemap);
	style.setDefault( "points" );
	logger.registerPluginParameter(style);

	decorate.setXMLPath(xml_base_path+"/Style/decorate");
	decorate.setDefault( "true" );
	logger.registerPluginParameter(decorate);

	pointsize.setXMLPath(xml_base_path+"/Style/point-size");
	pointsize.setDefault( "1.0" );
	logger.registerPluginParameter(pointsize);

	linewidth.setXMLPath(xml_base_path+"/Style/line-width");
	linewidth.setDefault( "1.0" );
	logger.registerPluginParameter(linewidth);

	grid.setXMLPath(xml_base_path+"/Style/grid");
	grid.setDefault( "false" );
	logger.registerPluginParameter(grid);
	
	
	string tag = "X-axis";
// 	axes.x.present = true;
	axes.x.symbol.setXMLPath(xml_base_path+"/"+tag+"/Symbol/symbol-ref");
	axes.x.min.setXMLPath(xml_base_path+"/"+tag+"/"+"minimum");
	axes.x.max.setXMLPath(xml_base_path+"/"+tag+"/"+"maximum");
	axes.x.logarithmic.setXMLPath(xml_base_path+"/"+tag+"/"+"logarithmic");
	axes.x.logarithmic.setDefault("false");
	logger.registerPluginParameter(axes.x.symbol);
	logger.registerPluginParameter(axes.x.min);
	logger.registerPluginParameter(axes.x.max);
	logger.registerPluginParameter(axes.x.logarithmic);

	// prepare for up to 10 y-axis elements
	tag = "Y-axis";
	int num_ycols = 10;
	axes.y.symbols.resize(num_ycols);
	for(uint i=0; i<num_ycols;i++){
		axes.y.symbols[i]->setXMLPath(xml_base_path+"/"+tag+"/Symbol["+to_str(i)+"]/symbol-ref");
		logger.registerPluginParameter(*axes.y.symbols[i]);
	}
	axes.y.min.setXMLPath(xml_base_path+"/"+tag+"/"+"minimum");
	axes.y.max.setXMLPath(xml_base_path+"/"+tag+"/"+"maximum");
	axes.y.logarithmic.setXMLPath(xml_base_path+"/"+tag+"/"+"logarithmic");
	axes.y.logarithmic.setDefault("false");
	logger.registerPluginParameter(axes.y.min);
	logger.registerPluginParameter(axes.y.max);
	logger.registerPluginParameter(axes.y.logarithmic);
	
	tag = "Color-bar";
	axes.cb.symbol.setXMLPath(xml_base_path+"/"+tag+"/Symbol/symbol-ref");
	logger.registerPluginParameter(axes.cb.symbol);

	axes.cb.min.setXMLPath(xml_base_path+"/"+tag+"/"+"minimum");
	logger.registerPluginParameter(axes.cb.min);
	axes.cb.max.setXMLPath(xml_base_path+"/"+tag+"/"+"maximum");
	logger.registerPluginParameter(axes.cb.max);

	axes.cb.logarithmic.setXMLPath(xml_base_path+"/"+tag+"/"+"logarithmic");
	axes.cb.logarithmic.setDefault("false");
	logger.registerPluginParameter(axes.cb.logarithmic);

	map<string, LoggerPlotBase::Palette> paletteMap;
	paletteMap["hot"]       = Palette::HOT;
	paletteMap["afmhot"]    = Palette::AFMHOT;
	paletteMap["rainbow"]   = Palette::RAINBOW;
	paletteMap["gray"]      = Palette::GRAY;
	paletteMap["ocean"]     = Palette::OCEAN;
	paletteMap["grv"]       = Palette::GRV;
	paletteMap["default"]   = Palette::DEFAULT;
	axes.cb.palette.setXMLPath(xml_base_path+"/"+tag+"/"+"palette");
	axes.cb.palette.setConversionMap( paletteMap );
	logger.registerPluginParameter(axes.cb.palette);

	axes.cb.palette_reverse.setXMLPath(xml_base_path+"/"+tag+"/"+"reverse-palette");
	axes.cb.palette_reverse.setDefault("false");
	logger.registerPluginParameter(axes.cb.palette_reverse);
	
	
	
	
};


void LoggerLinePlot::init() {
// 	cout << "LoggerLinePlot::init" << endl;
	LoggerPlotBase::init();
	// Check a suitable output is present
	const auto& writers = logger.getWriters();
	for (auto out : writers) {
		if (dynamic_pointer_cast<LoggerTextWriter>(out)) {
			writer = dynamic_pointer_cast<LoggerTextWriter>(out);
			if (writer->getOutputFormat() != LoggerTextWriter::OutputFormat::CSV || 
			(writer->getFileSeparation() != LoggerTextWriter::FileSeparation::NONE && writer->getFileSeparation() != LoggerTextWriter::FileSeparation::CELL) )
				writer = nullptr;
			else 
				break;
		}
	}
	
	if (!writer) {
		int ret = logger.addWriter(make_shared<LoggerTextWriter>(logger, LoggerTextWriter::OutputFormat::CSV));
		if (ret<0)
			throw string("Unable to create writer for LoggerLinePlot");
		writer =  dynamic_pointer_cast<LoggerTextWriter>(logger.getWriters()[ret]);
		if (!writer)
			throw string("Unable to create writer for LoggerLinePlot");
		writer->init();
	}
	

	// Check whether all required input is defined and read the column number	
	struct SymCol { string symbol; int* col; };
	vector<SymCol> all_sym_cols;
	
	if (!axes.x.symbol.isDefined())
		throw string( "LoggerLinePlot: X-axis symbol not defined" );
	all_sym_cols.push_back( { axes.x.symbol(), &axes.x.column_num } );

	axes.cb.defined = axes.cb.symbol.isDefined();
	if (axes.cb.defined)
		all_sym_cols.push_back( { axes.cb.symbol(), &axes.cb.column_num } );
	
	// count the number of defined symbols
	num_defined_symbols = 0;
	for (int i=0; i< axes.y.symbols.size(); i++) {
		if (axes.y.symbols[i]->isDefined())
			num_defined_symbols++;
		else
			break;
	}
// 	cout << "Found " << num_defined_symbols << " symbols to plot" << endl; 
	axes.y.column_nums.resize(num_defined_symbols);
	
	for (int i=0; i<num_defined_symbols; i++) {
		all_sym_cols.push_back( { axes.y.symbols[i](), &axes.y.column_nums[i] } );
	}
	
	for (auto& sym : all_sym_cols) {
		*sym.col = writer->getSymbolColumn(sym.symbol);
		if (*sym.col<1) {
			*sym.col = writer->addSymbol(sym.symbol);
		}
		// Gnuplot counts from 1
		*sym.col+=1;
	}
	
	gnuplot = make_shared<Gnuplot>();
	 
	if( timerange() == TimeRange::HISTORY && !history.isDefined() )
	throw string("LoggerLinePlot: TimeRange::History mode requires the specification of a history length (in units of simulation time).");
	
	// set the axis labels
	// x label
	axes.x.label = "\""+writer->getSymbolDescription(axes.x.symbol())+"\"";
	
	// y label
	axes.y.label = "\"";
	for (auto s : axes.y.symbols){
		if (s->isDefined()) {
			if ( axes.y.label.size() > 1 ) // cannot check for empty() because it already contains a '"' character
				axes.y.label += ", ";
			axes.y.label += writer->getSymbolDescription(s());
		}
	}
	axes.y.label += "\"";
	
	axes.cb.defined = axes.cb.symbol.isDefined();
	if (axes.cb.defined) {
		axes.cb.label = "\""+writer->getSymbolDescription(axes.cb.symbol())+"\"";
	}
	
}

void LoggerLinePlot::plot()
{
	// generate filename for plot output file
	string plotfilename;
	
	if (terminal() != Terminal::SCREEN) {
		string outputfilename_ext = terminal_file_extension[ terminal() ];
		ostringstream fn;
		string datafilename_base = writer->getDataFileBaseName();
		fn << datafilename_base << "_plot_";
		fn << axes.x.symbol() << "_";
		fn << axes.y.symbols[0]() << "_";
		if( axes.cb.symbol.isDefined() )
			fn << axes.cb.symbol() << "_";
		
		if ( file_numbering() == FileNumbering::SEQUENTIAL ){
			fn << setfill('0') << setw(5) << plot_num++; 
			//fn << setfill('0') << setw(5) << int(rint( SIM::getTime() / logger.timeStep()));
		}
		else
			fn << SIM::getTimeName();
		fn << "." << outputfilename_ext;
		
		plotfilename = fn.str();
	}
//	cout << "LoggerPlot: plotfilename " << plotfilename << endl;

	ostringstream ss;

	
	string terminal_str  = terminal_name[terminal()];
	if (!terminal_str.empty()) {
		ss << "set terminal " << terminal_str << " ";

		if( plotsize.isDefined() ){
			VINT size = plotsize();
			ss << "size " << size.x << ", " << size.y << ";\n";
		}
		
		ss << ";\n";
	}
	if (terminal() != Terminal::SCREEN) {
		ss << "set output '" << plotfilename << "';\n";
	}

	// Separator
	ss << "set datafile separator \""<< writer->getSeparator() <<"\";\n";
	
	if( grid() ){
		ss << "set grid;\n";
	}

	string min_x = "*";
	string max_x = "*";
	string min_y = "*";
	string max_y = "*";
	string min_cb = "*";
	string max_cb = "*";
	
	ostringstream oss;
	if( axes.x.min.isDefined() ){
		oss.clear(); oss.str("");
		oss << axes.x.min.get(SymbolFocus());
		min_x = oss.str();
	}
	if( axes.x.max.isDefined() ){
		oss.clear(); oss.str("");
		oss << axes.x.max.get(SymbolFocus());
		max_x = oss.str();
	}
	if( axes.y.min.isDefined() ){
		oss.clear(); oss.str("");
		oss << axes.y.min.get(SymbolFocus());
		min_y = oss.str();
	}
	if( axes.y.max.isDefined() ){
		oss.clear(); oss.str("");
		oss << axes.y.max.get(SymbolFocus());
		max_y = oss.str();
	}
	if( axes.cb.min.isDefined() ){
		oss.clear(); oss.str("");
		oss << axes.cb.min.get(SymbolFocus());
		min_cb = oss.str();
	}
	if( axes.cb.max.isDefined() ){
		oss.clear(); oss.str("");
		oss << axes.cb.max.get(SymbolFocus());
		max_cb = oss.str();
	}
	
	if( !decorate() ){
		ss << "unset xlabel;\n";
		ss << "unset ylabel;\n";
		ss << "unset xtics;\n";
		ss << "unset ytics;\n";
		ss << "unset cblabel;\n";
		ss << "unset key;\n";
		ss << "unset colorbox;\n";
		ss << "unset title\n";
	}
	else{
		string xlabel = axes.x.label;
		string ylabel = axes.y.label;
		string cblabel = axes.cb.label;
		ss << "set xlabel "<< xlabel <<";\n" ;
		ss << "set ylabel "<< ylabel <<";\n" ;
		if(axes.cb.symbol.isDefined()){
			ss << "set cblabel "<<  cblabel << ";\n";
			if( axes.cb.palette.isDefined() ){
				switch( axes.cb.palette() ){
					case( Palette::HOT ):
						ss << "set palette rgb 21,22,23;\n"; break;
					case(  Palette::AFMHOT ):
						ss << "set palette rgb 34,35,36;\n"; break;
					case(  Palette::OCEAN ):
						ss << "set palette rgb 23,28, 3;\n"; break;
					case(  Palette::GRV ):
						ss << "set palette rgb  3,11, 6;\n"; break;
					case( Palette::RAINBOW ):
						ss << "set palette rgb 33,13,10;\n"; break;
					case( Palette::GRAY ):
						ss << "set palette gray;\n"; break;
					default:
						ss << "set palette rgb  7, 5,15;\n"; break;
				}
				// TODO: import decent colorscales
// 				string palette = gnuplot->set_palette( int(axes.cb.palette()) );
// 				replace_substring(palette, "\\ \n", " ");
// 				ss << palette;
			}
			else {
				if (logger.getInput( axes.cb.symbol() )->flags().integer ) {
					int i = 0;
					std::map <double, string > color_template;
					color_template[i++]="red";
					color_template[i++]="yellow";
					color_template[i++]="dark-green";
					color_template[i++]="blue";
					color_template[i++]="orange";
					color_template[i++]="spring-green";
					color_template[i++]="light-coral";
					color_template[i++]="turquoise";
					color_template[i++]="dark-magenta";
					color_template[i++]="light-blue";
					
					// make a palette suitable for the value range
					int fsize = color_template.size();
					auto [cb_min, cb_max] = writer->getSymbolRange(axes.cb.symbol());
					if (axes.cb.min.isDefined()) cb_min = axes.cb.min(SymbolFocus::global);
					if (axes.cb.max.isDefined()) cb_max = axes.cb.max(SymbolFocus::global);
					int count = abs(cb_max-cb_min)+1;
					min_cb = to_str(cb_min-0.5);
					max_cb = to_str(cb_max+0.5);
					ss << "set palette maxcolors " << count << ";";
					ss << "set palette defined (";
					for ( i = 0;;) {
						ss << cb_min + i << " '" << color_template[i % fsize] << "' ";
						i++; 
						if (i > count) break;
						ss << ", ";
					}
					ss << ");\n";
				}
			}
			if( axes.cb.palette_reverse() )
				ss << "set palette negative;\n";
		}
		else
			ss << "unset colorbox;\n";

		// only show legend (key) when plotting multiple data columns
		if( axes.y.column_nums.size() == 1 )
			ss << "unset key;\n";
		
		if ( title.isDefined()) {
			ss << "set title \"" << title() << "\"\n";
		}
	}
	if(axes.x.logarithmic())
		ss << "set log x;\n";
	if(axes.y.logarithmic())
		ss << "set log y;\n";
	if(axes.cb.logarithmic())
		ss << "set log cb;\n";
	
	ss << "set cbrange["<< min_cb << ":" << max_cb << "];\n";


    // Style
	stringstream linespoints_ss;
	switch( style() ){
		case(Style::POINTS):{
			linespoints_ss << " with points pt 7 pointsize " << pointsize();
			break;
		}
		case(Style::LINES):{
			linespoints_ss << " with lines linewidth " << linewidth();
			break;
		}
		case(Style::LINESPOINTS):{
			linespoints_ss << " with linespoints pt 7 linewidth " << linewidth() << " pointsize " << pointsize();
			break;
		}
	}

    // Range
    stringstream datarange_ss;
    if( first_line.isDefined() || last_line.isDefined() || increment.isDefined() ){
        datarange_ss << "every ";
        if( increment.isDefined() )
            datarange_ss << int(increment.get( SymbolFocus() ));
        if( first_line.isDefined() )
            datarange_ss << "::" << int(first_line.get( SymbolFocus() ));
        if( last_line.isDefined() && !first_line.isDefined() )
            datarange_ss << "::";
        if( last_line.isDefined() )
            datarange_ss << "::" << int(last_line.get( SymbolFocus() ));
        datarange_ss << " ";
    }

    // The main plot command
    // TODO:: Here we must take care to pick the proper files and offsets for the plots,
    // also considering the data might be chunked into different files for timepoints and cells.
    
    if (writer->getFileSeparation() == LoggerTextWriter::FileSeparation::TIME || writer->getFileSeparation()== LoggerTextWriter::FileSeparation::TIME_CELL) {
		// just pick the proper time points to plot
		cout << "LoggerLinePlot::plot: missing implementation" << endl;
		assert(0);
	}
    else {
// 		if (writer->getFileSeparation() == LoggerTextWriter::FileSeparation::CELL) {
// 			cout << "LoggerLinePlot::plot: missing implementation" << endl;
// 			assert(0);
// 		}
		double time = SIM::getTime();
		vector<string> datafiles;
		
		if (writer->getFileSeparation() == LoggerTextWriter::FileSeparation::NONE)
			datafiles.push_back( std::get<0>(writer->getDataFile(time)) );
		else {
			datafiles = writer->getDataFiles();
		}

		
		// set range of time points to include in plot
		stringstream timerange_x;
		
		switch( timerange() ){
			case(TimeRange::ALL):{
				timerange_x << "($1 <= " << time << "?$" << (axes.x.column_num) << ":NaN)";
				break;
			}
			case(TimeRange::SINCELAST):{
				timerange_x << "($1 > " << last_plot_time << " && $1 <= " << time << "?$" << (axes.x.column_num) << ":NaN)";
				break;
			}
			case(TimeRange::HISTORY):{
				timerange_x << "($1 > " << (time-history.get( SymbolFocus())) << " && $1 <= " << time << "?$" << (axes.x.column_num) << ":NaN)";
				break;
			}
			case(TimeRange::CURRENT):{
				timerange_x << "($1 == " << time << "?$" << (axes.x.column_num) << ":NaN)";
				break;
			}
		}
		stringstream timerange_cb;
		if (axes.cb.defined) {
				switch( timerange() ){
				case(TimeRange::ALL):{
					timerange_cb << "($1 <= " << time << "?$" << (axes.cb.column_num) << ":NaN)";
					break;
				}
				case(TimeRange::SINCELAST):{
					timerange_cb << "($1 > " << last_plot_time << " && $1 <= " << time << "?$" << (axes.cb.column_num) << ":NaN)";
					break;
				}
				case(TimeRange::HISTORY):{
					timerange_cb << "($1 > " << (time-history.get( SymbolFocus())) << " && $1 <= " << time << "?$" << (axes.cb.column_num) << ":NaN)";
					break;
				}
				case(TimeRange::CURRENT):{
					timerange_cb << "($1 == " << time << "?$" << (axes.cb.column_num) << ":NaN)";
					break;
				}
			}
		}
		
		ss << "plot ["<< min_x <<":"<< max_x <<"]["<< min_y <<":"<< max_y <<"] ";
		
		for (uint f=0; f<datafiles.size(); f++) {
			for(uint c=0; c<num_defined_symbols; c++){
				if ( ! (c==0 && f== 0) )
					ss << ", ";
					
				if (c==0)
					ss << "'./" << datafiles[f] << "' ";
				else
					ss << "'' ";
				ss << datarange_ss.str() << " us " << timerange_x.str() << ":"<< axes.y.column_nums[c] <<":" << \
				( axes.cb.defined ? timerange_cb.str() : "(0)")  << \
				linespoints_ss.str() << (axes.cb.defined ? " pal":"") << " title \"" << writer->getSymbolDescription(axes.y.symbols[c]()) << "\"";
			}
		}
		ss << ";\n";
	}

	ss << "unset output;";

    // Write gnuplot commands to file
	if( logcommands() ){
		ofstream command_log;
        command_log.open((writer->getDataFileBaseName() + "_plot_commands.gp").c_str(),ios_base::out);
		command_log << ss.str();
		command_log.close();
	}

    // Finally, execute the plot commands in Gnuplot
	gnuplot->cmd( ss.str() );
}

LoggerMatrixPlot::LoggerMatrixPlot(Logger& logger, string xml_base_path): LoggerPlotBase(logger, xml_base_path)
{

	string tag = "Color-bar";
	cb_axis.symbol.setXMLPath(xml_base_path+"/"+tag+"/Symbol/symbol-ref");
	logger.registerPluginParameter(cb_axis.symbol);

	cb_axis.min.setXMLPath(xml_base_path+"/"+tag+"/"+"minimum");
	logger.registerPluginParameter(cb_axis.min);
	cb_axis.max.setXMLPath(xml_base_path+"/"+tag+"/"+"maximum");
	logger.registerPluginParameter(cb_axis.max);

	cb_axis.logarithmic.setXMLPath(xml_base_path+"/"+tag+"/"+"logarithmic");
	cb_axis.logarithmic.setDefault("false");
	logger.registerPluginParameter(cb_axis.logarithmic);

	map<string, LoggerPlotBase::Palette> paletteMap;
	paletteMap["hot"]       = Palette::HOT;
	paletteMap["afmhot"]    = Palette::AFMHOT;
	paletteMap["rainbow"]   = Palette::RAINBOW;
	paletteMap["gray"]      = Palette::GRAY;
	paletteMap["ocean"]     = Palette::OCEAN;
	paletteMap["grv"]       = Palette::GRV;
	paletteMap["default"]   = Palette::DEFAULT;
	cb_axis.palette.setXMLPath(xml_base_path+"/"+tag+"/"+"palette");
	cb_axis.palette.setConversionMap( paletteMap );
	logger.registerPluginParameter(cb_axis.palette);

	cb_axis.palette_reverse.setXMLPath(xml_base_path+"/"+tag+"/"+"reverse-palette");
	cb_axis.palette_reverse.setDefault("false");
	logger.registerPluginParameter(cb_axis.palette_reverse);
}

void LoggerMatrixPlot::init()
{
	LoggerPlotBase::init();
	// Check a suitable output is present
	const auto& writers = logger.getWriters();
	for (auto out : writers) {
		if (dynamic_pointer_cast<LoggerTextWriter>(out)) {
			writer = dynamic_pointer_cast<LoggerTextWriter>(out);
			if (writer->getOutputFormat() != LoggerTextWriter::OutputFormat::MATRIX)
				writer = nullptr;
			else 
				break;
		}
	}
	
	if (!writer) {
		int ret = logger.addWriter(make_shared<LoggerTextWriter>(logger, LoggerTextWriter::OutputFormat::MATRIX));
		if (ret<0)
		{
			throw string("Unable to create writer for LoggerMatrixPlot");
		}
		
		writer =  dynamic_pointer_cast<LoggerTextWriter>(logger.getWriters()[ret]);
		if (!writer)
			throw string("Unable to create writer for LoggerMatrixPlot");
		writer->init();
	}
	
	// Check whether all required input is defined
	if ( ! writer->getSymbol(cb_axis.symbol()) ) {
		writer->addSymbol(cb_axis.symbol());
	}
	
	
	gnuplot = make_shared<Gnuplot>();
	
	cb_axis.defined = cb_axis.symbol.isDefined();
	if( cb_axis.defined){
		cb_axis.label = writer->getSymbolDescription(cb_axis.symbol());
	}
	else
		throw string("Logger: Matrix plot requires specification of a symbol for the Colorbar");
}

void LoggerMatrixPlot::plot()
{
// 	cout << "LoggerMatrixPlot::plot" << endl;
	//throw MorpheusException("Logger/Plot: Plotting Matrix format is not implemented yet.", stored_node);

	// TODO: 
	// - Select proper input datafile according to Color-bar symbol (LoggerOutput needs store multiple output file names)
	// - Add option to select the relevant datablock (to feed to the gnuplot keyword 'index') with a MathExpression (datablocks can represent either time frames or z-slices (for 3D lattices))
	// - Add option to select colormap

	auto outer_range = FocusRange(logger.getGranularity(),logger.getRestrictions());
	if ( outer_range.dataAxis()[0] == FocusRangeAxis::CELL) {
		outer_range = FocusRange(Granularity::Cell,logger.getRestrictions());
	}
	else {
		outer_range = FocusRange(Granularity::Global,logger.getRestrictions());
	}
		
// #pragma omp parallel for if (outer_range.size()>1)
	for (auto focus : outer_range ) {
    // generate filename for plot output file
		auto [datafile, index] = writer->getDataFile(focus, SIM::getTime(),cb_axis.symbol());
		string outputfilename_ext = terminal_file_extension[ terminal() ];
		ostringstream fn;	
		fn << "logger_";
		if (logger.getInstanceNum()>1)
			fn << logger.getInstanceID() << "_";
		fn << cb_axis.symbol() << "_plot_";
		if (outer_range.size()>1) {
			fn << focus.cellID() << "_";
		}
		if ( file_numbering() == FileNumbering::SEQUENTIAL){
			fn << setfill('0') << setw(5) << plot_num++;
		}
		else
			fn << SIM::getTimeName();
		fn << "." << outputfilename_ext;
		string plotfilename = fn.str();


		ostringstream ss;
		string terminal_str = terminal_name[ terminal() ];
		if (!terminal_str.empty()) {
			ss << "set terminal " << terminal_str ;
			if( plotsize.isDefined() ){
				ss << " size " << plotsize().x << ", " << plotsize().y;
			}
			ss << ";\n";
		}
	
		if (terminal() != Terminal::SCREEN)
			ss << "set output '" << plotfilename << "';\n";

		// Separator
		ss << "set datafile separator \""<< writer->getSeparator() <<"\";\n";


		if ( cb_axis.palette.isDefined() ){
			switch( cb_axis.palette() ){
			case( Palette::HOT ):
				ss << "set palette rgb 21,22,23;\n"; break;
			case( Palette::AFMHOT ):
				ss << "set palette rgb 34,35,36;\n"; break;
			case( Palette::OCEAN ):
				ss << "set palette rgb 23,28, 3;\n"; break;
			case( Palette::GRV ):
				ss << "set palette rgb  3,11, 6;\n"; break;
			case( Palette::RAINBOW ):
				ss << "set palette rgb 33,13,10;\n"; break;
			case( Palette::GRAY ):
				ss << "set palette gray;\n"; break;
			default:
				ss << "set palette rgb  7, 5,15;\n"; break;
			}
		}
    
		if( cb_axis.palette_reverse() )
			ss << "set palette negative;\n";

		// TODO: get labels back

		ss << "set xlabel \"" << getLabels()[0] << "\";\n";
		ss << "set ylabel \"" << getLabels()[1] << "\";\n";
		ss << "set cblabel \"" << cb_axis.label << "\";\n";

		string cbmin= (cb_axis.min.isDefined() ? to_str(cb_axis.min.get(SymbolFocus())): "*");
		string cbmax= (cb_axis.max.isDefined() ? to_str(cb_axis.max.get(SymbolFocus())): "*");
		ss << "set xrange  [0:*];\n";
		ss << "set yrange  [*:*];\n";
		ss << "set cbrange [" << cbmin << ":" << cbmax << "];\n";

// TODO Fix Matrix Header and Block selection

		string nonuniform;
		// main plot command
		if( writer->hasHeader() )
			nonuniform = " nonuniform ";

/////  DATA BLOCK:
    // - first: 0
    // - last: if( separate_files ) STATS_black-2, else 0
    // - fixed: user-specified

    // if( last_block ) // get index of last data block
    //  if( separate_file == false ) 0
    //  else
    // ss << "stats \"" <<  writer->getDataFile(SIM::getTime(),cb_axis.symbol()) << "\";\n";
    // ss << "block = STATS_blocks-2;\n";
    // ss << "if(block<0) block = 0;\n";

    // if fixed data block
    //ss << "stats \"" << datafile << "\";\n";
    //ss << "if block = STATS_blocks-2;\n";
    //ss << "if( " << datablock() << " > last_block ) print \"Error: Specified data block not available in file."\"; else block=" << last_block << "; print \"All okay...\";\n";

//// MAIN PLOT COMMAND
    // PLOT
		if ( logger.getGranularity() == Granularity::MembraneNode) {
			if (MembraneProperty::lattice()->getDimensions() == 2) {
				ss << "set xrange  [0:2*pi];\n";
				ss << "set yrange  [-pi/2:pi/2];\n";
			}
			else {
				ss << "set xrange  [0:2*pi];\n";
			}
		}

		ss << "plot \"" << datafile << "\" index " << index << " " << nonuniform << " matrix " << /*" using " << s_using <<*/ " with image notitle;\n";
    ///// SPLOT with PM3D
    // Switch off interpolation
    //ss << "set pm3d map;\n";
    // ss << "set pm3d corners2color c1;\n";

    //ss << "splot \"" << datafilename_base << "\" " << nonuniform << " matrix index block notitle;\n";


    // Heatmap can also be plotted with plot (matrix with image).
    // However, this way, one cannot select a particular dataset (datablock).

////////////////////////////////////////////////////////////////////////////////////

		ss << "unset output;";

		// Write gnuplot commands to file
		if( logcommands() ){
			ofstream command_log;
			string filename = writer->getDataFileBaseName();
			filename += string("_") + cb_axis.symbol() + "_plot_commands";
			auto file_sep = writer->getFileSeparation();
			if (file_sep == LoggerTextWriter::FileSeparation::CELL || file_sep == LoggerTextWriter::FileSeparation::TIME_CELL) {
				filename += "_";
				filename += to_str(focus.cellID());
			}
			filename += ".gp";
			command_log.open(filename.c_str(),ios_base::out);
			command_log << ss.str();
			command_log.close();
		}

		// Finally, execute the plot commands in Gnuplot
		gnuplot->cmd( ss.str() );
	}

}



//// -------------------------------------------------------------------------------------------

vector<string> LoggerMatrixPlot::getLabels()
{
	Granularity granularity = logger.getGranularity();
	FocusRange range(granularity, logger.getRestrictions(), logger.getDomainOnly(), logger.getIncludeBoundary());

	vector<string> xy_labels;	
	switch( granularity ){
		case Granularity::Global:{
			// senseless to write Global to Matrix
			xy_labels.push_back( SymbolBase::Space_symbol+".x" );
			xy_labels.push_back( SymbolBase::Space_symbol+".y" );
			break;
		}
		case Granularity::Cell:{
			xy_labels.push_back( SymbolBase::CellID_symbol );
			xy_labels.push_back( SymbolBase::Time_symbol );
			break;
		}
		case Granularity::MembraneNode:{
			xy_labels.push_back( SymbolBase::MembraneSpace_symbol+".phi" );
			if(MembraneProperty::lattice()->getDimensions()>1)
				xy_labels.push_back( SymbolBase::MembraneSpace_symbol+".theta" );
			else
				xy_labels.push_back( SymbolBase::Time_symbol );
			break;
		}
		case Granularity::Node:{
			auto restrictions = logger.getRestrictions();
			// check whether there is a slice-restriction
			bool is_slice = false;
			FocusRangeAxis slice;
			for(auto restriction : restrictions){
				if( restriction.first == FocusRangeAxis::X ||
					restriction.first == FocusRangeAxis::Y ||
					restriction.first == FocusRangeAxis::Z ){
					is_slice = true;
					slice = restriction.first;
				}
			} 
			
			if( is_slice ){
				switch( slice ){
					case FocusRangeAxis::X:{
						// y is cols, z is rows
						xy_labels.push_back( SymbolBase::Space_symbol+".y" );
						if(range.dimensions()>1)
							xy_labels.push_back( SymbolBase::Space_symbol+".z" );
						else
							xy_labels.push_back( SymbolBase::Time_symbol );
						break;
					}
					case FocusRangeAxis::Y:{
						// x is cols, z is rows
						xy_labels.push_back( SymbolBase::Space_symbol+".x" );
						if(range.dimensions()>1)
							xy_labels.push_back( SymbolBase::Space_symbol+".z" );
						else
							xy_labels.push_back( SymbolBase::Time_symbol );
						break;
					}
					case FocusRangeAxis::Z:{
						// x is cols, y is rows
						xy_labels.push_back( SymbolBase::Space_symbol+".x" );
						if(range.dimensions()>1)
							xy_labels.push_back( SymbolBase::Space_symbol+".y" );
						else
							xy_labels.push_back( SymbolBase::Time_symbol );
						break;
					}
				}
			}
			else{
				xy_labels.push_back( SymbolBase::Space_symbol+".x" );
				if(range.dimensions()>1)
					xy_labels.push_back( SymbolBase::Space_symbol+".y" );
				else
					xy_labels.push_back( SymbolBase::Time_symbol );
			}
			break;
		}
	}
	for (auto& label : xy_labels) {
		label = writer->getSymbolDescription(label);
	}
	return xy_labels;
}
