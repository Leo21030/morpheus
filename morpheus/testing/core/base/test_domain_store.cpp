#include "gtest/gtest.h"
#include "core/domain_store.h"
#include <vector>


TEST (DomainStore, Insert) {
	auto domain = domain_store();
	domain.insert({2,1,0});
	domain.insert({1,2,0});
	domain.insert({3,2,0});
	domain.insert({4,1,0});
	domain.insert({1,1,0});
	domain.insert({3,1,0});
	domain.insert({2,2,0});
	domain.insert({2,0,0});
	domain.insert({1,0,0});

	domain.dump();

	vector<VINT> expect_1 {{1,0,0},{2,0,0},{1,1,0},{2,1,0},{3,1,0},{4,1,0},{1,2,0},{2,2,0},{3,2,0}};
	auto e = expect_1.begin();
	for (const auto d : domain) {
		EXPECT_EQ(d, *e);
		e++;
	}

};

TEST (DomainStore, Remove) {
	auto domain = domain_store();
	domain.insert({2,1,0});
	domain.insert({1,2,0});
	domain.insert({3,2,0});
	domain.insert({4,1,0});
	domain.insert({1,0,1});
	domain.insert({1,1,0});
	domain.insert({3,1,0});
	domain.insert({2,2,0});
	domain.insert({2,0,0});
	domain.insert({1,0,0});
	try {
		domain.remove({3,1,0});
		domain.remove({2,2,0});
		domain.remove({4,1,0});
		domain.remove({1,0,0});
	} catch (const string& e) {
		cout << e << endl;
	}
	
	
	domain.dump();

	vector<VINT> expect_1 {{2,0,0},{1,1,0},{2,1,0},{1,2,0},{3,2,0},{1,0,1}};
	auto e = expect_1.begin();
	for (const auto d : domain) {
		EXPECT_EQ(d, *e);
		e++;
	}

};

TEST (DomainStore, Iterator) {
	auto domain = domain_store();
	EXPECT_EQ(domain.begin(),domain.end());
	EXPECT_EQ(domain.end() - domain.begin(), 0);
	
	domain.insert({2,1,0});
	domain.insert({1,2,0});
	domain.insert({3,2,0});
	domain.insert({4,1,0});
	domain.insert({1,1,0});
	domain.insert({3,1,0});
	domain.insert({2,2,0});
	domain.insert({2,0,0});
	domain.insert({1,0,0});
	domain.insert({4,0,1});
	
	EXPECT_EQ(domain.end()-domain.begin(), 10);
	VINT sum {0,0,0};
	int count = 0;
	for (const auto& pos : domain) {
		++count;
		sum += pos;
	}
	EXPECT_EQ(count, 10);
	EXPECT_EQ(sum, VINT(23,10,1));
	
}
