
#include "gtest/gtest.h"
#include "model_test.h"
#include "core/simulation.h"
#include "core/focusrange.h"

const double mass_error_tolerance_factor = 1e-8;
const double operator_error_tolerance_factor = 2e-3;


TEST (MembraneDiffusion, Square) {
	
	auto file1 = ImportFile("membrane_diffusion_2D.xml");
	auto model = TestModel(file1.getDataAsString());
	
	model.run();
	
	auto initial_mass = SIM::findGlobalSymbol<double>("initial_mass") -> get(SymbolFocus::global);
	auto mass = SIM::findGlobalSymbol<double>("mass") -> get(SymbolFocus::global);
	EXPECT_NEAR(mass, initial_mass, initial_mass*mass_error_tolerance_factor);
	
	auto total_error = SIM::findGlobalSymbol<double>("total_error") -> get(SymbolFocus::global);
	EXPECT_NEAR(total_error/initial_mass, 0, operator_error_tolerance_factor);
}

TEST (MembraneDiffusion, Cubic) {
	
	auto file1 = ImportFile("membrane_diffusion_3D.xml");
	auto model = TestModel(file1.getDataAsString());
	
	model.run(4);
	auto initial_mass = SIM::findGlobalSymbol<double>("mass_f") -> get(SymbolFocus::global);
	
	model.run(14);
	auto mass_f = SIM::findGlobalSymbol<double>("mass_f") -> get(SymbolFocus::global);
	auto mass_fd = SIM::findGlobalSymbol<double>("mass_f") -> get(SymbolFocus::global);
	EXPECT_NEAR(mass_f, initial_mass, initial_mass*1e-3);
	EXPECT_NEAR(mass_fd, initial_mass, initial_mass*1e-3);
	
	// We remain very tolerant here, since the analytical solution currently implemented only nears the true solution 
	auto total_error_f = SIM::findGlobalSymbol<double>("total_error_f") -> get(SymbolFocus::global);
	auto total_error_fd = SIM::findGlobalSymbol<double>("total_error_fd") -> get(SymbolFocus::global);
	EXPECT_NEAR(total_error_f/initial_mass, 0, 5e-3);
	EXPECT_NEAR(total_error_fd/initial_mass, 0, 5e-3);
}

TEST (Membrane, PCP) {
	
	auto file1 = ImportFile("MembranePCP_4x4_and_mass_conservation.xml");
	auto model = TestModel(file1.getDataAsString());

	model.run();
	
	auto alignment = SIM::findGlobalSymbol<double>("Test.PCP") -> get(SymbolFocus::global);
	auto mass_dev = SIM::findGlobalSymbol<double>("Test.mass") -> get(SymbolFocus::global);
	
	EXPECT_LE(alignment, 1e-2);
	EXPECT_LE(mass_dev, 1e-10);
}
