SET(RULESETS 
# upgrades
	rules_2_3.xml
	rules_3_4.xml
	rules_4_5.xml
# downgrades
	rules_5_4.xml
)

FIND_PROGRAM(XSLTPROC "xsltproc" NAMES "xsltproc xsltproc.exe" DOC "XSLTProc executable")
IF( NOT XSLTPROC )
	message(FATAL_ERROR "Unable to locate 'xsltproc' executable")
ENDIF()

SET(RULEPROC rules2cpp.xslt)

foreach(RULESET ${RULESETS}) 
	SET(cpp_rules "${RULESET}.cpp")
	message(STATUS "Creating rule set ${cpp_rules}")
	ADD_CUSTOM_COMMAND( 
		OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${cpp_rules}
		COMMAND ${XSLTPROC} -o ${CMAKE_CURRENT_BINARY_DIR}/${cpp_rules} ${RULEPROC} ${RULESET}
		DEPENDS ${RULEPROC} ${RULESET}
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		COMMENT "Generating MorpheusML conversion rules ${RULESET}" VERBATIM
	)
	STRING(APPEND RULE_INCLUDES "#include \"${cpp_rules}\" \n")
	LIST(APPEND RULE_DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${cpp_rules})
endforeach()

add_custom_target(MorpheusMLconv_dep
DEPENDS ${RULE_DEPENDS})

configure_file(morpheusML_rules.cpp.cmake morpheusML_rules.cpp @ONLY)
ADD_LIBRARY(MorpheusMLconv ${CMAKE_CURRENT_BINARY_DIR}/morpheusML_rules.cpp ml_conversion.cpp)
ADD_DEPENDENCIES(MorpheusMLconv MorpheusMLconv_dep)
target_link_libraries_patched(MorpheusMLconv PRIVATE XMLUtils)
target_include_directories(MorpheusMLconv PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(MorpheusMLconv PRIVATE ${CMAKE_CURRENT_BINARY_DIR} #[[${PROJECT_SOURCE_DIR}/morpheus]])
SET_TARGET_PROPERTIES(MorpheusMLconv PROPERTIES RESOURCE "MorpheusML-4.1.xsd")


