//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef string_functions_h
#define string_functions_h



// #include "config.h"
#include <vector>
#include <deque>
#include <map>
#include <string>
#include <iostream>
#include <sstream>
#include <regex>
#include <cstdlib>
#include <iomanip>
#include <iterator>
#include <algorithm>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/join.hpp>



#define c_array_size( a )  (sizeof(a)/sizeof(a[0]))
#define c_array_begin( a ) (a)
#define c_array_end( a )   (a+c_array_size(a))

using std::string;
using std::vector;
using std::map;
using std::deque;

namespace str {
	using boost::join;
	using boost::trim;
	using boost::replace_all;
	enum SplitBehavior { SkipEmptyParts, KeepEmptyParts};
	/// Split a string into tokens separated by chars. If SkipEmptyParts is set, all empty tokens are discarded
	std::vector<string> split(const string& val, const string& chars, SplitBehavior empty_parts = SkipEmptyParts);
	
	/// Replace the '%i' sequence with the lowest integet i with repl. If non is found, no replacement takes place.
	string& arg(string& val, const string& repl);
	
	bool contains(const string& val, const string& sub);
	bool startsWith(const string& val, const string& sub);
	bool endsWith(const string& val, const string& sub);
	
	string& remove_all_of(string& val, const string& chars);
	string& remove(string& val, const string& chars);
	
	
};

/// additional output operators for loading and storing data
std::ostream& operator << (std::ostream& out, bool b);
std::istream& operator >> (std::istream& in, bool& b);

// output operators for Vectors
template <class T>
std::ostream& operator << (std::ostream& out, vector<T> v) {
	copy(v.begin(), v.end(), std::ostream_iterator<T>(out,";"));
	return out;
}

template <class T>
std::istream& operator >> (std::istream& in, vector<T>& v) {
	T tmp;
	while (1) {
		in >> tmp;
		if ( in.fail() ) break;
		v.push_back(tmp);
		char sep;
		in >> sep;
		if (sep != ';' or in.bad()) break;
	}
	return in;
}

/// additional output operators for loading and storing data
std::istream& operator >> (std::istream& stream, std::deque<double>& q);
std::ostream& operator << (std::ostream& stream, const std::deque<double>& q);

// output operators for MAPS
template <class T1, class T2>
std::ostream& operator << (std::ostream& out, std::map<T1,T2> m) {
	copy(m.begin(), m.end(), std::ostream_iterator<std::pair<T1,T2> >(out,";"));
	return out;
}
template <class T1, class T2>
std::istream& operator >> (std::istream& in, std::map<T1,T2>& m) {
	std::pair<T1,T2> tmp;
	while (1) {
		in >> tmp;
		if ( in.fail() ) break;
		m[tmp.first] = tmp.second;
		char sep;
		in >> sep;
		if (sep != ';' or in.fail()) break;
	}
	return in;
}


template <class T1, class T2>
std::ostream& operator << (std::ostream& out, std::pair<T1,T2> p) {
	out << p.first << " => " << p.second;
	return out;
}

template <class T1, class T2>
std::istream& operator >> (std::istream& in, std::pair<T1,T2>& p) {
	string sep;
	in >> p.first >> sep >> p.second;
	return in;
}

string strip_last_token(string& str, const string& del="/");
string remove_spaces(string& str);
string replace_spaces(string& str, char replacement='_');
bool replace_substring(std::string& str, const std::string& from, const std::string& to);
vector<string> tokenize(const string& str, const string& delimiters = " ", bool drop_empty_tokens = false);

using boost::join;
using boost::trim;

string& lower_case(string& a);
string lower_case(const char* a);


// template <class T>
// bool from_cstr(T value, const char* cstr) {
// 	stringstream s(cstr);
// 	s >> value;
// }

#endif
